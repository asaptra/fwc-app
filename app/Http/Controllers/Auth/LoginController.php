<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

use App\Models\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function loginMetamask(Request $request)
    {
        $fwcAddress = $request->input('fwcAddress');
    
        $checkUser = User::where('fwc_address',$fwcAddress)->first();
        $email = '';
        $password = '';
        $user = '';

        if(!$checkUser)
        {
            $email = substr(str_shuffle('abcdefghijklmnopqrstuvwxyz0123456789'),1,10).'@'.str_repeat('fwc', 1).'.com';
            $password = Hash::make('12341234');

            $user = User::create([
                'name' => 'fwcUserx',
                'email' => $email,
                'password' => $password,
                'fwc_address' => $fwcAddress,
                'refer_code' => $this->randomString()
            ]);

            
        }
        else
        {
            $email = $checkUser->email;
            $user = User::where('email', $email)->first();
            
            Auth::login($user);
            return response()->json([
                'success' => true
            ], 200);
        }
        
        if(Auth::attempt(['email' => $email, 'password' => '12341234'])) {
            return response()->json([
                'success' => true
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => $user->toJson()
            ], 401);
        }
        
    }

    private function randomString() 
    {
        $alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }

        return implode($pass);
    }

}
