<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Transaction;
use App\Models\ConfigPrice;
use App\Models\TokenHolder;
use Auth;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $configPrice = ConfigPrice::first();
        $fwcUsdPrice = $configPrice->price_usd_fwc;
        $fwcSellPrice = $configPrice->price_sell_fwc;
        $fwcRemainingToken = $configPrice->fwc_remaining_token;
        $assetFund = $configPrice->asset_fund;
        $colleteralBallance = $configPrice->colleteral_ballance;
        $fwcExchangeAddress = $configPrice->fwc_exchange_address;
        $usdtExchangeAddress = $configPrice->usdt_exchange_address;

        $tokenHolder = TokenHolder::all()->sum('balance');
        $remainingToken = 4000000 - $tokenHolder;
        $collBalance = $fwcSellPrice * $tokenHolder;
       
        $transactions = Transaction::where('fwc_address', Auth::user()->fwc_address)->get();
        $leftFwcWhenProgress = Transaction::where('fwc_address', Auth::user()->fwc_address)
                                ->where('trans_type',1)
                                ->where('status',0)
                                ->sum('total_fwc_send');

        return view('home', compact('fwcUsdPrice','fwcRemainingToken','assetFund','colleteralBallance','transactions','leftFwcWhenProgress','fwcExchangeAddress','fwcSellPrice','remainingToken','collBalance','usdtExchangeAddress'));
    }

    public function profile()
    {
        return view('admin.profile');
    }

    public function editProfile()
    {
        $user = User::find(Auth::user()->id);

        return view('admin.editprofile', compact('user'));
    }

    public function submitProfile(Request $request)
    {
        $validate = $request->validate([
            'fwc_address' => 'required|unique:users,fwc_address,'.$request->id,
            'email' => 'required|unique:users,email,'.$request->id
        ]);

        $incomingUserId = $request->id;
        $currentUserLogin = Auth::user()->id;

        if($incomingUserId == $currentUserLogin)
        {
            $filename = null;
            $path = null;

            if(!empty($request->profile_image))
            {
               $request->validate([
                    'profile_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ]);
            
                $file = $request->file('profile_image');
                $filename = date('YmdHi').$file->getClientOriginalName();
                $file->move(public_path('photo/profile/images'), $filename);
            }

            $user = User::find($currentUserLogin);
            $user->fwc_address = $request->fwc_address;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->image_name = $filename;
            $user->image_path = $path;
            $user->save();

            return Redirect::back()->with('success','Profile has been updated');;
        }

        return Redirect::back()->with('failed','Failed edit profile');
    }


    public function referral()
    {
         return view('admin.referral');
    }

    public function submitReferral(Request $request)
    {
        $current_referral = Auth::user()->refer_code;
        $user = [];

        if($request->refer_code_registered != $current_referral)
        {
            $user = User::where('refer_code', $request->refer_code_registered)
                    ->first();

            if($user)
            {
                $current_user = User::find(Auth::user()->id);
                $current_user->refer_code_registered = $user->refer_code;
                $current_user->save();

                $user->refer_code_used = intval($user->refer_code_used + 1);
                $user->save();

                return Redirect::back()->with('success','Succeed');
            }
            else
            {
                return Redirect::back()->with('message', 'Invalid referral code');
            }
            
        }
        else
        {
            return Redirect::back()->with('message','Invalid referral code');
        }
    }

    public function submitBuyFwc(Request $request)
    {
        try 
        {
            $priceFwc = ConfigPrice::first()->price_usd_fwc;
           
            $trans = new Transaction();
            $trans->fwc_address = Auth::user()->fwc_address;
            $trans->fwc_price = $priceFwc;
            $trans->trans_type = 0; //buy
            $trans->total_usdt = $request->total_usdt;
            $trans->total_fwc_send = $request->total_usdt / $priceFwc;
            $trans->status = 0; //on progress
            $trans->response = $request->response;
            $trans->tx_hash = $request->tx_hash;
            $trans->save();

            return Redirect::back()->with('success','Transaction Succeed');
            
        }
        catch (exception $e) {
            return Redirect::back()->with('failed','Failed Transaction');
        }
    }

    public function submitSellFwc(Request $request)
    {
        try
        {
            $priceFwc = ConfigPrice::first()->price_sell_fwc;
           
            $trans = new Transaction();
            $trans->fwc_address = Auth::user()->fwc_address;
            $trans->fwc_price = $priceFwc;
            $trans->trans_type = 1; //sell
            $trans->total_usdt = $request->total_fwc * $priceFwc;
            $trans->total_fwc_send = $request->total_fwc;
            $trans->status = 0; //on progress
            $trans->usdt_address = $request->usdt_address; //on progress
            $trans->response = $request->response;
            $trans->tx_hash = $request->tx_hash;
            $trans->save();

            return Redirect::back()->with('success','Transaction Succeed');
            
        }
        catch (exception $e) {
            return Redirect::back()->with('failed','Failed Transaction');
        }
    }

    public function changePassword(Request $request)
    {
         $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ]);
   
        User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);

        return Redirect::back();
    }

    public function transaction()
    {
        if(Auth::user()->roles != 'admin')
        {
            return Redirect::back()->with('failed', 'Invalid roles system');
        }

        $transactions = Transaction::all();
        
        return view('transaction', compact('transactions'));
    }

    public function adminTransfer(Request $request)
    {
        if(Auth::user()->roles != 'admin')
        {
            return Redirect::back()->with('failed', 'Invalid roles system');
        }
        
        $trans = Transaction::find($request->id);
        $fwc_address = $trans->fwc_address;
        
        $trans->status = 1; //completed
        $trans->response = $request->response;
        $trans->tx_admin = $request->tx_admin;
        $trans->save();

        $tokenHolder = TokenHolder::where('holder_address',$fwc_address)->first();
        if($request->trans_type == 0)
        {
            if($tokenHolder)
            {
                $current_balance = $tokenHolder->balance;
                $tokenHolder->balance = $current_balance + $request->amount;
                $tokenHolder->save();
            }
            else
            {
                $tokenHolder = new TokenHolder();
                $tokenHolder->holder_address = $fwc_address;
                $tokenHolder->balance = $request->amount;
                $tokenHolder->save();
            }
        }
        else
        {
            $configPrice = ConfigPrice::first();
            $fwcSellPrice = $configPrice->price_sell_fwc;
            $amountFwc = $request->amount/$fwcSellPrice;
            $current_balance = $tokenHolder->balance;
            
            $tokenHolder->balance = $current_balance - $amountFwc;
            $tokenHolder->save();
        }
        

        return Redirect::back()->with('success','Transaction Succeed');
    }

    public function configPrice()
    {
        if(Auth::user()->roles != 'admin')
        {
            return Redirect::back()->with('failed', 'Invalid roles system');
        }

        $cp = ConfigPrice::first();
        
        return view('admin.configprice', compact('cp'));
    }

    public function submitConfigPrice(Request $request)
    {
        if(Auth::user()->roles != 'admin')
        {
            return Redirect::back()->with('failed', 'Invalid roles system');
        }

        $configPrice = ConfigPrice::first();
        $configPrice->fwc_exchange_address = $request->fwc_exchange_address;
        $configPrice->usdt_exchange_address = $request->usdt_exchange_address;
        $configPrice->price_sell_fwc = $request->price_sell_fwc;
        $configPrice->price_usd_fwc = $request->price_usd_fwc;
        $configPrice->save();

        return Redirect::back()->with('success','Config Succeed');
    }
}
