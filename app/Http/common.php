<?php 

function current_user()
{
    return Auth::user();
}

function getRemainingToken()
{
    $totSupply = 4000000;
    $tokenHolder = App\Models\TokenHolder::all()->sum('balance');
    $remainingToken = $totSupply - $tokenHolder;

    return $remainingToken;
}