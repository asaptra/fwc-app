<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConfigPrice extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'price_usd_fwc',
        'fwc_remaining_token',
        'asset_fund',
        'colleteral_ballance',
        'fwc_exchange_address',
        'usdt_exchange_address',
        'config_prices'
    ];

}
