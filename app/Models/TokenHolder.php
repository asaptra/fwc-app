<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TokenHolder extends Model
{
    use HasFactory;

    protected $fillable = [
        'holder_address',
        'balance',
    ];
}
