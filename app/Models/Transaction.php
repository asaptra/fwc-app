<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
     use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'fwc_address',
        'fwc_price',
        'trans_type',
        'total_usdt',
        'total_fwc_send',
        'status',
        'proof_image',
        'usdt_address',
        'response',
        'tx_hash',
        'tx_admin'
    ];
}