<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_prices', function (Blueprint $table) {
            $table->id();
            $table->decimal('price_usd_fwc', 10,2)->default(0);
            $table->decimal('fwc_remaining_token', 10,2)->default(0);
            $table->decimal('asset_fund', 10,2)->default(0);
            $table->decimal('colleteral_ballance', 10,2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_prices');
    }
}