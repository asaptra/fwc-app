<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->string('fwc_address')->nullable();
            $table->integer('trans_type')->nullable();
            $table->decimal('fwc_price', 10,2)->nullable();
            $table->decimal('total_usdt', 10,2)->nullable();
            $table->decimal('total_fwc_send', 10,2)->nullable();
            $table->integer('status')->nullable();
            $table->string('proof_image')->nullable();
            $table->string('usdt_address')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
