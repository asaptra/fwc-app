$('.toggle-sidebar').click(function(){
    $('#sidebar').toggleClass('active')
    $('#main').toggleClass('active')
})
 var mainHeight = $("#main").height() + 100;
    $("#sidebar").css("height", mainHeight);

if (window.matchMedia("(max-width: 1000px)").matches) {
    $('#sidebar').addClass('active')
    $('#main').addClass('active')
  } else {
    $('#sidebar').removeClass('active')
    $('#main').removeClass('active')
  }

window.addEventListener("resize", function() {
  if (window.matchMedia("(max-width: 1000px)").matches) {
    $('#sidebar').addClass('active')
    $('#main').addClass('active')
  } else {
    $('#sidebar').removeClass('active')
    $('#main').removeClass('active')
  }
})