$(".language .dropdown-menu input").click(function() {
    var selText = $(this).attr('value');
    $(this).parents('.language').find('.dropdown-toggle span').html(selText)
});

//top menu
if ($(this).scrollTop() > 30) {
    $('.fwc-navbar-front').addClass('fixed');
} else {
    $('.fwc-navbar-front').removeClass('fixed');
}
$(window).scroll(function() {
    if ($(this).scrollTop() > 30) {
        $('.fwc-navbar-front').addClass('fixed');
    } else {
        $('.fwc-navbar-front').removeClass('fixed');
    }
});

AOS.init({
    duration: 1000, // values from 0 to 3000, with step 50ms
    easing: 'ease', // default easing for AOS animations
    once: true, // whether animation should happen only once - while scrolling down
    mirror: true, // whether elements should animate out while scrolling past them
    disable: 'mobile'
});

$(document).ready(function() {
    $('#nav-icon1').click(function() {
        $(this).toggleClass('open');
    });
});