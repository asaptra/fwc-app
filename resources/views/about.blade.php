@extends('layouts.public')

@section('content')
   @include('layouts.public_header')
   
   <section class="fwc-banner-inside about-bg">
         <div class="container-fluid centered">
            <div class="row d-flex justify-content-center">
               <div class="col-md-8 text-center" data-aos="fade-up">
                  <div>
                     <h6 class="fwc-subtitle-blue">BACKGROUND STORY</h6>
                     <h2>The Futureworkscoin Token started with a clear vision to create innovative ways for investors to see tangible benefits in an ethical way; helping you earn money that is backed by a diversified portfolio.</h2>
                     <p class="my-4">
                        The mission behind our project reflects the desire to create virtual token backed by a strong underlying portfolio, enabling the network to achieve more stability and sustainability for investing in the future. Using quant-based technology for our underlying portfolio enables us to grow future projects and minimize your investment risk.
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="fwc-vision-mission">
         <div class="container-fluid centered">
            <div class="row d-flex justify-content-between my-4">
               <div class="col-md-5">
                  <h3>Vision</h3>
                  <p>Endeavoring to create an ethical and financial ecosystem that supports local development in fiannce, education, technology and the environment.</p>
               </div>
               <div class="col-md-5">
                  <h3>Mission</h3>
                  <p>We want to create a system of safer investment that is backed by sustainable projects; for investors, for consumers, for the future.</p>
               </div>
            </div>
         </div>
      </section>
      <section class="fwc-promise" id="milestone-01">
         <div class="container-fluid centered">
            <div class="row d-flex align-items-center mt-5">
               <div class="col-md-6" data-aos="fade-left"><img src="assets/img/fwc-promise.png" /></div>
               <div class="col-md-6" data-aos="fade-right">
                  <h3>Our Promise
                  </h3>
                  <h6 class="fwc-subtitle-blue">Your Sustainable Independent Wealth</h6>
                  <p>Allowing all people to be part of a shared mission and to have a full control of their funds with our buy back mechanism that will keep the liquidity of the assets
                  </p>
               </div>
            </div>
         </div>
      </section>
<section class="fwc-we-offer my-5">
         <div class="container-fluid">
            <div class="row">
               <div class="col-md-12 text-center" data-aos="fade-up">
                  <h6 class="fwc-subtitle-blue">WHAT WE OFFER TO YOU</h6>
                  <h3>Gain biggest reward with your very low impact risk is made possible. </h3>
               </div>
            </div>
            <div class="row">
               <div class="col-md-4" data-aos="fade-up">
                  <div class="fwc-box-gradient">
                     <img src="assets/img/icon-growth.png" />
                     <p>Growth</p>
                     <p>Everyone wants to grow their wealth portfolio, which is why so many take measured risks and diversify through a variety of investment channels. FWC aims to create a more stable marketplace where our partners can gain significant returns in a safer environment
</p>
                  </div>
               </div>
               <div class="col-md-4" data-aos="fade-up" 
                  data-aos-delay="200">
                  <div class="fwc-box-gradient">
                     <img src="assets/img/icon-transparancy.png" />
                     <p>Transparency and Safety</p>
                     <p>Understanding how your investment is utilized is important. Our commitment to transparency means partners have access to regular updates - keeping you informed and helping you sleep better at night while your investment is growing</p>
                  </div>
               </div>
               <div class="col-md-4" data-aos="fade-up" 
                  data-aos-delay="400">
                  <div class="fwc-box-gradient">
                     <img src="assets/img/icon-market-liquidation.png" />
                     <p>Market Liquidation</p>
                     <p>FWC offers an easily accessible investment through deposit and withdrawal - backed by a buy-back mechanism.

  </p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="fwc-leadership my-5">
         <div class="container-fluid">
            <div class="row">
               <div class="col-md-12 text-center my-4" data-aos="fade-up">
                  <h3>Leadership </h3>
               </div>
            </div>
            <div class="row">
               <div class="col-md-6 my-4" data-aos="fade-up">
                  <div class="d-flex">
                     <img src="assets/img/leader-01.png" />
                     <div class="fwc-leadership-desc">
                        <h5>David</h5>
                        <p>Having been in the Investment industry for more than 15 years, David has a passion in doing research in developing his approach in trading. Combining his experiences in IT, David developed a innovative method of quantitative trading - and this is where everything started.</p>
                     </div>
                  </div>
               </div>
               <div class="col-md-6 my-4" data-aos="fade-up">
                  <div class="d-flex">
                     <img src="assets/img/leader-02.png" />
                     <div class="fwc-leadership-desc">
                        <h5>Yusuf Seto</h5>
                        <p>Yusuf has more than 15 years experience in senior management positions combined with a passion for marketing that has led Yusuf to work at a plethora of multinational companies throughout his professional career.</p>
                     </div>
                  </div>
               </div>
               <div class="col-md-6 my-4" data-aos="fade-up">
                  <div class="d-flex">
                     <img src="assets/img/leader-03.png" />
                     <div class="fwc-leadership-desc">
                        <h5>Patrick Compau</h5>
                        <p>An experienced management professional specializing in nonprofits, training, and education. He is passionate about social impact and believes that businesses, just like people can and should create positive change in their communitie and views FutureWorks Coin as a fantastic opportunity for others to contribute.</p>
                     </div>
                  </div>
               </div>
               <div class="col-md-6 my-4" data-aos="fade-up">
                  <div class="d-flex">
                     <img src="assets/img/leader-04.png" />
                     <div class="fwc-leadership-desc">
                        <h5>Pierre Edgcumbe</h5>
                        <p>Pierre has a passion for the development nations and the people living in them. He has been a senior management leader in the education sector for over 10 years and has been at the interchange of social impact and education, having set up and managed partnerships between education foundations and private enterprises.</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      
      @include('layouts.public_footer')
     
@endsection
