@extends('layouts.app')

@section('content')

<div class="row">
   <h4>Config Prices</h4>

  <div class="col-md-6">
   
    <form method="POST" action="{{ route('submit.configprice') }}" enctype="multipart/form-data">

      <div class="modal-body">
        @csrf
        <div class="form-group">
          <label>FWC Exchange Address</label>
          <input id="fwc_exchange_address" type="text" class="form-control @error('fwc_exchange_address') is-invalid @enderror" name="fwc_exchange_address" value="{{ $cp->fwc_exchange_address }}" required>

          @error('fwc_exchange_address')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <br/>
        <div class="form-group">
          <label>USDT Exchange Address</label>
          <input id="usdt_exchange_address" type="text" class="form-control @error('usdt_exchange_address') is-invalid @enderror" name="usdt_exchange_address" value="{{ $cp->usdt_exchange_address }}" required>

          @error('usdt_exchange_address')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <br/>
        <div class="form-group">
          <label>Price Buy (USDT)</label>
          <input id="price_usd_fwc" type="text" class="form-control @error('price_usd_fwc') is-invalid @enderror" name="price_usd_fwc" value="{{ $cp->price_usd_fwc }}" required>

          @error('price_usd_fwc')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <br/>
        <div class="form-group">
          <label>Price Sell (USDT)</label>
          <input id="price_sell_fwc" type="text" class="form-control @error('price_sell_fwc') is-invalid @enderror" name="price_sell_fwc" value="{{ $cp->price_sell_fwc }}" required>

          @error('price_sell_fwc')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>

      </div>
      <div class="modal-footer">
        <a href="/home" class="btn btn-secondary">Back</a>
        <button type="submit" class="btn btn-purple">Save changes</button>
      </div>
    </form>
  </div>
</div>

@endsection
