@extends('layouts.app')

@section('content')

<div class="row">
   <h4>Edit Profile</h4>

  <div class="col-md-6">
   
    <form method="POST" action="{{ route('submit.profile') }}" enctype="multipart/form-data">

      <div class="modal-body">
        @csrf
        <div class="form-group">
          <label>FWC Address</label>
          <input id="fwc_address" type="text" class="form-control @error('fwc_address') is-invalid @enderror" name="fwc_address" value="{{ $user->fwc_address }}" required>

          @error('fwc_address')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <br/>
        <div class="form-group">
          <label>Name</label>
          <input id="id" type="hidden" class="form-control @error('id') is-invalid @enderror" name="id" value="{{ $user->id }}" required>
          <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $user->name }}" required autocomplete="name" autofocus>

          @error('name')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <br/>
        <div class="form-group">
          <label>Email Address</label>
          <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $user->email }}" required autocomplete="email">

          @error('email')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <br/>
        <div class="form-group">
          <label>Photo Profile</label>
          <input id="profile_image" type="file" class="form-control @error('profile_image') is-invalid @enderror" name="profile_image">
        </div>            
      </div>
      <div class="modal-footer">
        <a href="/profile" class="btn btn-secondary">Back</a>
        <button type="submit" class="btn btn-purple">Save changes</button>
      </div>
    </form>
  </div>
</div>

@endsection
