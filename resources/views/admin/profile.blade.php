@extends('layouts.app')

@section('content')

 <section class="login register"  data-aos="fade-up">
      <div class="row d-flex justify-content-center">
         <div class="col-md-12">
            <div class="row">
               <div class="col-md-6">
                  <h4>My Profile</h4>

                  <table class="table text-light">
                   
                    <tbody>
                      <tr>
                        <td>FWC Address</td>
                        <td></td>
                        <td>{{ Auth::user()->fwc_address }}</td>
                      </tr>
                      <tr>
                        <td>Name</td>
                        <td></td>
                        <td>{{ Auth::user()->name }}</td>
                      </tr>
                      <tr>
                        <td>Email</td>
                        <td></td>
                        <td>{{ Auth::user()->email }}</td>
                      </tr>
                      <tr>
                        <td>Referal Code</td>
                        <td></td>
                        <td>{{ Auth::user()->refer_code }}</td>
                      </tr>
                     </tbody>
                  </table>
                  <a href="/edit_profile" class="btn btn-purple open-edit-profile" >Edit Profile</a>
                  <button type="button" class="btn btn-purple" data-bs-toggle="modal" data-bs-target="#changePasswordModal">Change Password</button>
                  
               </div>
            </div>
      </div>
   </div>
</section>
      

<!-- Modal -->
<div class="modal fade text-light" id="editProfileModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content" style="background-color: #29243b;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Profile</h5>
        <button type="button" class="btn-close bg-light" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
       <form method="POST" action="{{ route('submit.profile') }}" enctype="multipart/form-data">
       
      <div class="modal-body">
         @csrf
         <div class="form-group">
            <label>FWC Address</label>
            <input id="fwc_address" type="text" class="form-control @error('fwc_address') is-invalid @enderror" name="fwc_address" value="{{ old('fwc_address') }}" required>

            @error('fwc_address')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
         </div>
         <br/>
         <div class="form-group">
            <label>Name</label>
            <input id="id" type="hidden" class="form-control @error('id') is-invalid @enderror" name="id" value="{{ old('id') }}" required>
            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

            @error('name')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
         </div>
         <br/>
         <div class="form-group">
            <label>Email Address</label>
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

              @error('email')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
         </div>
         <br/>
         <div class="form-group">
            <label>Photo Profile</label>
            <input id="profile_image" type="file" class="form-control @error('profile_image') is-invalid @enderror" name="profile_image">
         </div>            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-purple">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>


<div class="modal fade text-light" id="changePasswordModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content" style="background-color: #29243b;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Change Password</h5>
        <button type="button" class="btn-close bg-light" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
       <form method="POST" action="{{ route('change.password') }}">
       
      <div class="modal-body">
         @csrf
         <div class="form-group">
            <div class="d-flex justify-content-between"><label>Current Password</label></div>
            <input id="current_password" type="password" class="form-control @error('current_password') is-invalid @enderror" name="current_password" required autocomplete="new-password">

              @error('current_password')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
            
         </div>
         <br/>
         <div class="form-group">
            <div class="d-flex justify-content-between"><label>New Password</label></div>
            <input id="new_password" type="password" class="form-control @error('new_password') is-invalid @enderror" name="new_password" required autocomplete="new-password">

              @error('new_password')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $new_password }}</strong>
                  </span>
              @enderror
            
         </div>
         <br/>
         <div class="form-group">
            <div class="d-flex justify-content-between"><label>Confirm New Password</label></div>

            <input id="new_confirm_password" type="password" class="form-control" name="new_confirm_password" required autocomplete="new-password">
            
         </div>          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-purple">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   
<script type="text/javascript">
  $(document).on("click", ".open-edit-profile", function () {
     var email = '{{ Auth::user()->email }}';
     var idUser = '{{ Auth::user()->id }}';
     var name = '{{ Auth::user()->name }}';
     var fwc_address = '{{ Auth::user()->fwc_address }}';
    
     $("#editProfileModal .modal-body #email").val( email );
     $("#editProfileModal .modal-body #id").val( idUser );
     $("#editProfileModal .modal-body #name").val( name );
     $("#editProfileModal .modal-body #fwc_address").val( fwc_address );
});

</script>
@endsection
