@extends('layouts.app')

@section('content')

 <section class="login register"  data-aos="fade-up">
         <div class="container-fluid centered">
            <div class="row d-flex justify-content-center">
               <div class="col-md-6">
                  <div class="row">

                    <div class="card text-center text-dark bg-warning">
                      <div class="card-header">
                        My Referral Code
                      </div>
                      <div class="card-body ">
                        <h5 class="card-title">{{ Auth::user()->refer_code }}</h5>
                       
      <input type="text" value="{{ Auth::user()->refer_code }}" id="myInput" style="display:none;">
       <br/>

                        <button onclick="myFunction()" class="btn btn-purple" id="btn-copy">Copy text</button>


                      </div>
                      <div class="card-footer text-muted">
                       {{ Auth::user()->refer_code_used }} Referral code used
                      </div>
                    </div>

                  </div>
               </div>


            </div>
            <br/> 
             <div class="row d-flex justify-content-center">
               <div class="col-md-6">
                  <div class="row">

                    <div class="card text-center text-dark bg-light">
                      <div class="card-header">
                        Submit Referral Code
                      </div>
                      <div class="card-body ">
                         @if(empty(Auth::user()->refer_code_registered))
                     
                        <p class="card-text">Input your referral code for getting reward!</p>
              
                      <form method="POST" action="{{ route('submit.referral') }}">
                       @csrf
                       
                        <input type="text"  id="refer_code_registered" name="refer_code_registered" class="form-control" required="true" value="{{ old('refer_code_registered') }}">
     
                        <span class="text-danger">{{ session()->get('message') }}</span>
                        <br/>
                        <br/>

                        <button class="btn btn-purple" type="submit">Submit</button>
                      </form>
                      @else

                        <p class="card-text">You already use referral :  {{ Auth::user()->refer_code_registered }}</p>
                      @endif

                      </div>
                    </div>

                  </div>
               </div>


            </div>

            
         </div>
      </section>
     

      <script>
      function myFunction() {

        var copyText = document.getElementById("myInput");


        copyText.select();
        copyText.setSelectionRange(0, 99999);
        navigator.clipboard.writeText(copyText.value);
        

        
      }

      const btn = document.getElementById("btn-copy");

        btn.addEventListener("click", ()=>{

            if(btn.innerText === "Copied"){
                btn.innerText = "Already Copied!";
            }else{
                btn.innerText= "Copied";
            }
        });
      </script>
@endsection
