@extends('layouts.public')

@section('content')

@include('layouts.public_header')
    <section class="login" data-aos="fade-up">
     <div class="container-fluid centered">
        <div class="row d-flex justify-content-center">
           <div class="col-md-6">

              <form method="POST" action="{{ route('login') }}">
                  @csrf

                <h4>Login</h4>
                <div id="webOnly"> 
                  <a class="btn btn-white" id="loginButton"><img src="assets/img/icon-metamask.png"/> Login via Metamask</a>
                </div>
                
                <div id="mobileOnly">
               </div>

                <p id='userWallet' class='text-lg text-gray-600 my-2'></p>
                 <span>OR</span>
                 <div class="form-group">
                    <label>Email Address</label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                 </div>
                 <div class="form-group">
                    <div class="d-flex justify-content-between"><label>Password</label><a href="/password/reset">Forgot Password?</a></div>
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                 </div>
                 <div class="d-flex justify-content-end">
                    <button class="btn btn-purple" type="submit">Login</button>
                 </div>
              </form>
              <div class="text-center my-4">
                 <p>Don’t have an account? </p>
                 <a href="/register" class="btn btn-outline d-block w-75" style="margin:0 auto">Create an account</a>
              </div>
           </div>
        </div>
     </div>
    </section>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <!-- Bootstrap core JS-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script src="https://unpkg.com/@metamask/detect-provider/dist/detect-provider.min.js"></script>

    <script src="assets/js/main.js"></script>
    <script src="assets/js/login_metamask.js"></script>
@endsection
