@extends('layouts.public')

@section('content')

@include('layouts.public_header')
    <section class="login" data-aos="fade-up">
     <div class="container-fluid centered">
        <div class="row d-flex justify-content-center">
           <div class="col-md-6">

                <form method="POST" action="{{ route('password.email') }}">
                    
                  @csrf

                 <h4>Reset Password</h4>
                 <div class="form-group">
                    <label>Email Address</label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                 </div>
                  <div class="d-flex justify-content-end">
                    <button class="btn btn-purple" type="submit">Send Password Reset Link</button>
                 </div>
              </form>
              
           </div>
        </div>
     </div>
    </section>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <!-- Bootstrap core JS-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script src="../assets/js/main.js"></script>
  
@endsection

