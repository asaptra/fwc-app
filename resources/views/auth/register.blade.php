@extends('layouts.public')

@section('content')

@include('layouts.public_header')
 <section class="login register"  data-aos="fade-up">
         <div class="container-fluid centered">
            <div class="row d-flex justify-content-center">
               <div class="col-md-10">
                  <div class="row">
                     <div class="col-md-6 px-4">
                        <h4>Create an account</h4>
                        <a class="btn btn-white" id="loginButton"><img src="assets/img/icon-metamask.png"/> Register via Metamask</a>
                        <p id='userWallet' class='text-lg text-gray-600 my-2'></p>
                     </div>
                     <div class="col-md-6">
                        <form method="POST" action="{{ route('register') }}">
                        @csrf
                           <div class="form-group">
                              <label>Name</label>
                              <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                              @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                           </div>
                           <div class="form-group">
                              <label>Email Address</label>
                              <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                           </div>
                           <div class="form-group">
                              <div class="d-flex justify-content-between"><label>Password</label></div>
                              <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                              
                           </div>
                           <div class="form-group">
                              <div class="d-flex justify-content-between"><label>Confirm Password</label></div>

                              <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                              
                           </div>
                        
                           <div class="form-group">
                                <div class="d-flex justify-content-between"><label>Captcha</label></div>
                                
                                <div class="row">
                                  <div class="col-md-8 captcha">
                                      <span>{!! captcha_img() !!}</span>
                                  </div>
                                  <div class="col-md-2 mt-3">
                                     <button type="button" class="btn btn-danger" class="reload" id="reload">
                                      &#x21bb;
                                      </button>
                                  </div>
                                 </div>
                           </div>

                        <div class="form-group">
                            <label for="captcha" class="col-md-4 col-form-label text-md-right">Enter Captcha</label>
                                <input id="captcha" type="text" class="form-control" name="captcha">

                                @error('captcha')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                           <div class="d-flex justify-content-end">
                              <button class="btn btn-purple" type="submit">Register</button>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row d-flex justify-content-end">
               <div class="col-md-6">
                  <div class="my-4 d-flex justify-content-between align-items-baseline">
                     <p>Already have an account ? </p>
                     <a href="/login" class="btn btn-outline w-25" style="margin:0 auto">Login</a>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
        <script src="assets/js/main.js"></script>
        <script src="assets/js/login_metamask.js"></script>

        <script type="text/javascript">
         $( document ).ready(function() {
             $(".captcha span img").css({"width": "300px", "margin-left": "-15px", "display": "block"});

         });
            

          $('#reload').click(function () {
              $.ajax({
                  type: 'GET',
                  url: 'reload-captcha',
                  success: function (data) {
                      $(".captcha span").html(data.captcha);
                      $(".captcha span img").css({"width": "300px", "margin-left": "-15px", "display": "block"});
                  }
              });
          });
      </script>
@endsection
