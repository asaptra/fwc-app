@extends('layouts.public')

@section('content')
    @include('layouts.public_header')
    <section class="fwc-banner-inside faq-bg">
         <div class="container-fluid centered">
            <div class="row d-flex">
               <div class="col-md-7" data-aos="fade-up">
                  <div class="fwc-banner-text">
                     <h6 class="fwc-subtitle-blue">FREQUENTLY ASKED QUESTIONS</h6>
                     <h2>FAQ</h2>
                     <p class="my-4">
                        Browse through these FAQs to find answers to commonly raised questions. Or you can contact us if you have any further questions.
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="fwc-faq-question">
         <div class="container-fluid centered">
            <div class="row d-flex justify-content-center">
               <div class="col-md-10" data-aos="fade-up">
                  <div class="accordion accordion-flush" id="accordionFlushExample">
                     <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingOne">
                           <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="true" aria-controls="flush-collapseOne">
                           1. What is cryptocurrency?
                           </button>
                        </h2>
                        <div id="flush-collapseOne" class="accordion-collapse collapse show" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample" style="">
                           <div class="accordion-body">Refers to a digital currency, secured with cryptography to enable trusted transactions. Blockchain is the underlying technology, functioning as a ‘ledger’ or record of transactions made.<br><br>Hundreds of currencies are in circulation, such as Bitcoin, Ether, Monero, etc. Each is designed by one or more brilliant individuals, usually meant to run as a decentralised system so that no single entity can control it.<br/><br/>
                           Cryptocurrency units are usually generated on the basis of an algorithm announced to everyone in advance, by ‘miners’ using powerful computers. Having expended a lot of time and electricity on ‘mining’, these miners can hold on to the units or sell to others.
                           </div>
                        </div>
                     </div>
                     <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingTwo">
                           <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                           2. How is crypto stored?
                           </button>
                        </h2>
                        <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                           <div class="accordion-body">Let’s look at a national currency like the rupee. It can be deposited in your name at a bank, or privately stuffed into a mattress at home far away from anyone’s eyes.<br/><br/>
                              Similarly, a cryptocurrency can be held on your behalf by a company, usually in your wallet at a crypto exchange online. You could also hold it in without being affiliated to anybody, in a private cryptocurrency wallet.
                           </div>
                        </div>
                     </div>
                     <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree">
                           <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                           3. What is the purpose of cryptocurrency?
                           </button>
                        </h2>
                        <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                           <div class="accordion-body">As indicated by ‘currency’, they were originally intended to be used in the same way as rupees and dollars are, as a medium of payment between people for products and services purchased.<br/><br/>

                           Consider store reward cards, an alternative physical payment method that is denominated in their own units, and not in national currency. Similarly, cryptocurrency with its own units was meant to enable easy digital transactions online, at lower costs than what conventional banks charged.</div>
                        </div>
                     </div>
                     <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingFour">
                           <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false" aria-controls="flush-collapseFour">
                           4. Is cryptocurrency used for illegal activities?
                           </button>
                        </h2>
                        <div id="flush-collapseFour" class="accordion-collapse collapse" aria-labelledby="flush-headingFour" data-bs-parent="#accordionFlushExample">
                           <div class="accordion-body">
                              Initially with no government control, crypto became a useful tool to escape political censors and repressive regimes, which was an admirable goal. However, crypto eventually became known as a method of transacting for illegal substances on hidden parts of the internet.
                              <br/><br/>
                              Governments discourage such behaviour and made use of crypto’s built-in ledger to pursue criminals. With the extent of tracking that is now possible in 2021, it is safe to say that it is difficult to use cryptocurrency for crime. Bitcoin for instance, sees over 300,000 transactions daily on average, with crypto exchange trades accounting for over half of them in the last two years.
                           </div>
                        </div>
                     </div>
                     <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingFive">
                           <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFive" aria-expanded="false" aria-controls="flush-collapseFive">
                           5. How does supply and demand work in the cryptocurrency market?
                           </button>
                        </h2>
                        <div id="flush-collapseFive" class="accordion-collapse collapse" aria-labelledby="flush-headingFive" data-bs-parent="#accordionFlushExample">
                           <div class="accordion-body">
                           Some cryptocurrencies like Bitcoin and Ether are designed to have a limited supply. By comparison, real-world currencies like the US Dollar do not have a hard limit on supply. When demand increases, the value of a supply-limited item is expected to increase.<br/><br/>
                           That difference in supply, a high demand for crypto and new ways to profit from rising crypto, have led to a self-perpetuating cycle that drives up the exchange value of major cryptocurrencies.
                           </div>
                        </div>
                     </div>
                     <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingFive">
                           <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFive" aria-expanded="false" aria-controls="flush-collapseFive">
                              6. Who creates cryptocurrencies?
                           </button>
                        </h2>
                        <div id="flush-collapseFive" class="accordion-collapse collapse" aria-labelledby="flush-headingFive" data-bs-parent="#accordionFlushExample">
                           <div class="accordion-body">
                           Cryptocurrencies are usually created by developers and entrepreneurs with various political or economic visions.Bitcoin was started in 2009 by someone under the pseudonym Satoshi Nakamoto who has largely remained anonymous. Ethereum was created by Toronto native Vitalik Buterin in 2015 to complement bitcoin and enable automatic business payments. Software engineer Billy Markus created dogecoin in 2013, mostly as a joke.
                           </div>
                        </div>
                     </div>
                     <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingFive">
                           <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFive" aria-expanded="false" aria-controls="flush-collapseFive">
                              7. Where is cryptocurrency stored?
                           </button>
                        </h2>
                        <div id="flush-collapseFive" class="accordion-collapse collapse" aria-labelledby="flush-headingFive" data-bs-parent="#accordionFlushExample">
                           <div class="accordion-body">
                           Cryptocurrency isn’t technically stored anywhere. It’s not saved in a folder or on a hard drive. Evidence of how much cryptocurrency you hold is stored on the blockchain. The ledger is updated across the network with each new transaction — when a new bitcoin is mined as well as when someone moves their cryptocurrency.<br/><br/>
                           To access your personal cryptocurrencies, you need a private key or complicated password that’s generated by code when you create a wallet. In bitcoin, the private key is a 256-bit password, which is cryptography language meaning there could be dozens of characters in a seemingly endless number of variations.
                           The private key creates a unique signature that enables you to use your cryptocurrency to make transactions.
                           <br/><br/>
                           The private key also correlates to a public key, which miners can see, and a bitcoin address, which you can think of as similar to a public bank account number. The address is a unique, 26- to 35-character, case-sensitive string of letters and numbers, showing where cryptocurrency is sent on the blockchain. The private keys can be stored inside specialized virtual wallets, which are apps offered by crypto exchanges. You get a wallet when you sign up to buy cryptocurrency.
                           <br/><br/>
                           The complex passwords can also be saved in hardware wallets, or on a smartphone or computer. You can also print out a copy of the keys to store in a safe place.
                           The crypto wallets differ from the smartphone wallet you might be storing your debit and credit card information in. They’re often encrypted, and if you lose your password you can be locked out of your cryptocurrency forever.

                           </div>
                        </div>
                     </div>

                     <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingFive">
                           <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFive" aria-expanded="false" aria-controls="flush-collapseFive">
                              8. How is cryptocurrency passed among people and businesses?
                           </button>
                        </h2>
                        <div id="flush-collapseFive" class="accordion-collapse collapse" aria-labelledby="flush-headingFive" data-bs-parent="#accordionFlushExample">
                           <div class="accordion-body">
                           While traditional payment systems rely on banks to verify transactions, cryptocurrency transactions are verified by miners on the blockchain. Miners run mathematical checks to make sure that a transaction is valid, and a majority of the nodes must agree that it was a valid transaction before it’s added to the blockchain. Most people rely on crypto exchange services like Coinbase, eToro, Binance or Robinhood to buy and sell cryptocurrency. People can also give their bitcoin to others, similar to how you would transfer money to someone else’s bank account.<br/><br/>
                           As more companies embrace cryptocurrency, people are able to do even more with it. Some companies such as AT&T and CheapAir.com now accept cryptocurrency as tender. You can now use cryptocurrency to pay your phone bill or buy travel tickets.

                           </div>
                        </div>
                     </div>

                     <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingFive">
                           <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFive" aria-expanded="false" aria-controls="flush-collapseFive">
                              9. What government regulations exist?
                           </button>
                        </h2>
                        <div id="flush-collapseFive" class="accordion-collapse collapse" aria-labelledby="flush-headingFive" data-bs-parent="#accordionFlushExample">
                           <div class="accordion-body">
                           Part of the reason cryptocurrency has become more popular is that it’s not controlled by the Federal Reserve or any other agency within the government. It is, however, subject to taxes in circumstances laid out by the Internal Revenue Service in 2014 or any country regulation.<br/><br/>
                           Generally, taxpayers are expected to convert their cryptocurrency transactions into U.S. dollars to report gains and losses to the IRS. However, thousands of people and exchanges do not. Cryptocurrencies generate a lot of money, sometimes trillions of dollars in trading transactions, which is partially why the Senate wants to squeeze revenue out of the market to support its infrastructure plan.<br/><br/>
                           The Senate proposed a revised IRS tax-reporting requirement for crypto transactions on Sunday, Aug. 1, calling for “brokers” to report transactions above $10,000 to the IRS. While it was meant to target customer-facing platforms and crypto exchanges, it sparked concerns that the reporting rules were too broad and would apply to cryptocurrency miners, who don’t have customers.

                           </div>
                        </div>
                     </div>

                     <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingFive">
                           <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFive" aria-expanded="false" aria-controls="flush-collapseFive">
                              10. Will more regulation affect investors?
                           </button>
                        </h2>
                        <div id="flush-collapseFive" class="accordion-collapse collapse" aria-labelledby="flush-headingFive" data-bs-parent="#accordionFlushExample">
                           <div class="accordion-body">
                           It could.
                           While lawmakers are rewording who’s targeted under its plan to collect $28 billion in taxes, crypto crackdowns are known to push businesses to countries with more lax rules. If that happens in the U.S., it might rock confidence in the industry and cause investors to pull out, critics say.
                           At the same time, government oversight may make some people more comfortable investing since crypto trading gets a bad reputation as a haven for money laundering.
                           “For people who invest in the stock market, regulation adds familiarity that going to make them more comfortable with (crypto trading,)” said David Sacco, an economics professor at the University of New Haven.
                           It remains unclear how successful lawmakers will be at getting more companies to report crypto income since many are drawn to the sector due to its lack of government oversight. “It’ll be hard to target decentralized platforms run by code,” said David Yermack, a finance professor at New York University.
                           Beyond taxes, exchanging crypto is largely unregulated on the federal level, although some states like Wyoming and Ohio have made moves to welcome it locally. Wyoming signed into law a “Utility Token Bill” making it easier to operate a blockchain business while Ohio allows companies to pay a variety of taxes with cryptocurrency.


                           </div>
                        </div>
                     </div>


                     <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingFive">
                           <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFive" aria-expanded="false" aria-controls="flush-collapseFive">
                              11. What is the difference between a "Coin" and a "Token" on the site?
                           </button>
                        </h2>
                        <div id="flush-collapseFive" class="accordion-collapse collapse" aria-labelledby="flush-headingFive" data-bs-parent="#accordionFlushExample">
                           <div class="accordion-body">
                           A <i>Coin</i> is a cryptocurrency that can operate independently.<br/><br/>
                           A <i>Token</i> is a cryptocurrency that depends on another cryptocurrency as a platform to operate


                           </div>
                        </div>
                     </div>

                  </div>
               </div>
            </div>
         </div>
      </section>
    @include('layouts.public_footer')
@endsection