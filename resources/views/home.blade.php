@extends('layouts.app')

@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    
<script src="https://unpkg.com/moralis/dist/moralis.js"></script>
<script src="https://cdn.jsdelivr.net/npm/web3@latest/dist/web3.min.js"></script>

<script src="https://unpkg.com/@metamask/detect-provider/dist/detect-provider.min.js"></script>

    <script type="text/javascript">

        function formatToCurrency(amount){
    return (amount).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'); 
}

        var maxfwc =  0;
       
      window.addEventListener('load', function() {

        if (typeof web3 !== 'undefined') {
          console.log('Web3 Detected! ' + web3.currentProvider.constructor.name)
          // window.web3 = new Web3(web3.currentProvider);
         // window.ethereum.enable();
         const accounts = ethereum.request({ method: 'eth_accounts' });
         var priceFwc = {{$fwcUsdPrice}};
          (async () => {
            
           document.getElementById("fwc-price").innerHTML = 'USDT '+ formatToCurrency((await getBalance() - {{$leftFwcWhenProgress}}) * priceFwc);
           document.getElementById("fwc-balance").innerHTML = (await getBalance() - {{$leftFwcWhenProgress}}) + ' FWC';

           maxfwc = await getBalance() - {{$leftFwcWhenProgress}};
         })()
         
        } else {
          console.log('No Web3 Detected... get Metamask');
        }
      })

         const testnet = 'https://speedy-nodes-nyc.moralis.io/85651f3e6612d7d97bf48571/bsc/mainnet';
         const web3 = new Web3(new Web3.providers.HttpProvider(testnet));

         // The minimum ABI required to get the ERC20 Token balance
         const minABI = [
           {
             constant: true,
             inputs: [{ name: "_owner", type: "address" }],
             name: "balanceOf",
             outputs: [{ name: "balance", type: "uint256" }],
             type: "function",
           },
         ];

         const ABI = [
            {
                "type"            : "function",
                "name"            : "totalSupply",
                "inputs"          : [],
                "outputs"         : [{"name":"","type":"uint256"}],
                "stateMutability" : "view",
                "payable"         : false,
                "constant"        : true // for backward-compatibility
            }
        ];
         
         const tokenAddress = "0x7D059FafBF681AdBbf543d30260f3702D211b07e";
         const walletAddress = '{{Auth::user()->fwc_address}}';
         const contract = new web3.eth.Contract(minABI, tokenAddress);
         // const contract2 = new web3.eth.Contract(ABI, tokenAddress);
         async function getBalance() {
             // const supply = await contract2.methods.totalSupply().call(); 
             // console.log('-----');
             // console.log(supply);
           const result = await contract.methods.balanceOf(walletAddress).call(); // 29803630997051883414242659
           const format = web3.utils.fromWei(result); // 29803630.997051883414242659
           return await format;
         }

        
    </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.1/chart.min.js" integrity="sha512-QSkVNOCYLtj73J4hbmVoOV6KVZuMluZlioC+trLpewV8qMjsWqlIQvkn1KGX2StWvPMdWGBqim1xlC8krl1EKQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<div class="row">
   <div class="col-md-9">
      <div class="card card-grey">
         <div class="row">
         <div class="col-md-6 est-assets">
            <h6>ESTIMATED ASSETS VALUE</h6>
            <h4 id="fwc-price"></h4>
            <br/>
            <br/>
                
         </div>
         <div class="col-md-6">
            <div class="d-flex total-statistic fwc-balance">
               <div class="icon-box"><i class="icon-ic-logo"></i></div>
               <div>
                  <p>MY FWC Balance</p>
               <h4 id="fwc-balance"></h4>
               </div>
            </div>

            <div class="d-flex total-statistic fwc-remaining-token">
               <div class="icon-box"><i class="icon-ic-coins"></i></div>
               <div>
                  <p>FWC REMAINING TOKEN</p>
               <h4>{{number_format($remainingToken,3,".",",") }} FWC</h4>
               </div>
            </div>

            <div class="d-flex total-statistic fwc-collateral">
               <div class="icon-box"><i class="icon-ic-money"></i></div>
               <div>
                  <p>FUTURE WORK COLLATERAL BALANCE</p>
               <h4>USD ${{number_format($collBalance,2,".",",") }} </h4>
               </div>
            </div>
         </div>
      </div>
      </div>
   </div>
   <div class="col-md-3">
                  <div class="card card-grey">
                     <h6>FWC BUY BACK SCHEDULE</h6>
                     <ul class="buyback-schedule">
                        <li class="soon">
                           WED, 20 MAR 2022 
                        </li>
                        <li>
                           TUE, 5 JUL 2022
                        </li>
                        <li>
                           NEXT SCHEDULE
                        </li>
                         <li>
                           NEXT SCHEDULE
                        </li>
                     </ul>
                  </div>
               </div>
</div>
<div class="row mt-3">
               <div class="col-md-12 mb-2"><h6>FWC WOW/INDEX</h6></div>
               <div class="col-md-4">
                 <div class="d-flex card-index">
                    <div class="icon-box ic-buy"><i class="icon-ic-buy"></i></div>
                    <div>
                       <label>BUY FWC</label>
                       <h4>USDT {{ number_format($fwcUsdPrice,0,".",",")}} </h4>
                    </div>
                    <a href="$" class="arrow-send" data-bs-toggle="modal" data-bs-target="#buyModal"><i class="icon-ic-arrow-right-Stroke"></i></a>
                 </div>
               </div>
               <div class="col-md-4">
                 <div class="d-flex card-index">
                    <div class="icon-box ic-sell"><i class="icon-ic-sell"></i></div>
                    <div>
                       <label>SELL FWC</label>
                       <h4>USDT {{ number_format($fwcSellPrice,0,".",",")}}</h4>
                    </div>
                    <a  href="#" class="arrow-send"  data-bs-toggle="modal" data-bs-target="#sellModal"><i class="icon-ic-arrow-right-Stroke"></i></a>
                 </div>
               </div>
               <div class="col-md-4">
                 <div class="d-flex  card-index align-items-center">
                    <div class="icon-box ic-send"><i class="icon-ic-send"></i></div>
                    <div>
                       <label class="mb-0">SEND FWC</label>
                    </div>
                    <a  href="#" class="arrow-send"  data-bs-toggle="modal" data-bs-target="#transferModal"><i class="icon-ic-arrow-right-Stroke"></i></a>
                 </div>
               </div>
            </div>
<br/>
<div class="row">
    <div class="col-md-12">
      <div class="card card-chart">
         <h6>FWC PRICE CHART</h6>
         <div class="d-flex justify-content-between my-4">
            <div>
               
          <div class="dropdown account">
               <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
               
           <div class="coin-icon">
               <img src="assets/img/coin-icon.png" />
            </div>
            <p class="mx-3">FWC / USDT</p>
            <i class="icon-ic-arrow-caret-down-Stroke"></i>
               </button>
               <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton1">
            <a class="nav-link" href="#">USDT</a>
               </div>
            </div>
            </div>
            <ul class="select-time">
               <li>
                  <a href="">1M</a>
               </li>
            </ul>
         </div>
         <div class="chart-trading">
            <canvas id="myChart" width="400" height="100"></canvas>
         </div>
      </div>
   </div>
</div>
<br/>
<div class="row">
   <div class="col-md-12">
      <div class="card card-grey">
         <h6>HISTORY TRANSACTIONS (BUY/SELL)</h6>
         <table class="table text-light text-center">
             <thead>
               <th>Trans Type</th>
               <th>Date</th>
               <th>USDT</th>
               <th>FWC</th>
               <th>Status</th>
             </thead>
              <tbody>
                @foreach($transactions as $t)
                <tr>
                    <td>
                        @if ($t->trans_type == 0)
                            <span class="badge bg-success">BUY</span>
                        @endif
                        @if ($t->trans_type == 1)
                            <span class="badge bg-danger">SELL</span>
                        @endif
                    </td>
                    <td>{{$t->created_at}}</td>
                    <td>$ {{$t->total_usdt}}</td>
                    <td>{{$t->total_fwc_send}}</td>
                    <td>
                        @if ($t->status == 0)
                            <span class="badge bg-warning text-dark">On Progress</span>
                        @endif
                        @if ($t->status == 1)
                            <span class="badge bg-success">Completed</span>
                        @endif
                        @if ($t->status == 2)
                            <span class="badge bg-danger">Failed</span>
                        @endif
                    </td>
                </tr>
                @endforeach
               
               </tbody>
         </table>
      </div>
   </div>
</div><!-- 
linear-gradient(180deg, #2F274D 0%, #222336 100%) -->

<div class="modal fade text-light" id="transferModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" >
    <div class="modal-content" style="background-color: #29243b;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Transfer FWC Token</h5>
        <button type="button" class="btn-close bg-light" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
       
      <div class="modal-body">
         <div class="form-group">
            <div class="d-flex justify-content-between"><label>Receiver Address</label></div>
            <input id="fwc_receiver_address" type="text" class="form-control" name="fwc_receiver_address" required>
         </div>
         <br/>
         <div class="form-group">
            <div class="d-flex justify-content-between"><label>Amount</label></div>
            <input id="fwc_amount" type="number" class="form-control " name="fwc_amount" required>
         </div>
       
      <div id="demo" class="text-danger"></div>        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-purple" id="transferFwcButton">Transfer</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade text-light" id="buyModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content" style="background-color: #29243b;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Confirm BUY FWC Token</h5>
        <button type="button" class="btn-close bg-light" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
           <div class="modal-body">
             <div class="form-group">
                <div class="d-flex justify-content-between"><label>USDT</label></div>
                <input id="total_usdt" type="number" class="form-control " name="total_usdt" required>
                  <div id="fwc_received" class="text-danger"></div>
             </div>
          </div>
            <div id="demo-buy" class="text-danger"></div>
            <br/>
            <span>Make sure you have enough USDT (BEP20) in your Metamask wallet</span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-purple" id="buyFwcUsdtButton">Confirm</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade text-light" id="sellModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content" style="background-color: #29243b;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Confirm SELL FWC Token</h5>
        <button type="button" class="btn-close bg-light" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
           <div class="modal-body">
            <div class="form-group">
                <div class="d-flex justify-content-between"><label>Your USDT Address</label></div>
                <input id="usdt_address" type="text" class="form-control" name="usdt_address" required>
                <input id="fwc_exchange_address" type="hidden" class="form-control text-secondary" name="fwc_exchange_address" required value="{{$fwcExchangeAddress}}" readonly>
             </div>
             <br/>
             <div class="form-group">
                <div class="d-flex justify-content-between"><label>FWC</label></div>
                <input id="total_fwc" type="number" class="form-control" name="total_fwc" pattern="^[1-9]\d*$" required>
                  <div id="usdt_received" class="text-danger"></div>
             </div>
          </div>
           <div id="demo-sell" class="text-danger"></div>
         <br/>
      <span>Sell will follow the buy back schedule</span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-purple" id="btnSellFwc">Confirm Sell</button>
      </div>
    </div>
  </div>
</div>



<script>
    const ctx = document.getElementById('myChart');
    const myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ['Jan', 'Feb', 'Mar', 'Apr'],
            datasets: [{
                label: '1 FWC = 25 USDT',
                data: [25, 25, 25, 25, 25, 25],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: false
                }
            }
        }
    });
</script>


<script type="text/javascript">
    

    $( document ).ready(function() {

        const serverUrl = "https://berx8b2icccr.usemoralis.com:2053/server";
        const appId = "WEOKYUkhg64mkzpPWpDdGiLv8EgcLbHzFAr0lByI";

        Moralis.start({ serverUrl, appId });
    });

    $("#transferFwcButton").click(function() {
        var receiver = $('.modal-body #fwc_receiver_address').val();
        var value = $('.modal-body #fwc_amount').val();

        if(receiver.length == 0){
            $('.modal-body #fwc_receiver_address').after('<div class="text-danger">*Receiver address is required</div>');
        }

        if(value.length == 0){
            $('.modal-body #fwc_amount').after('<div class="text-danger">*Amount is required</div>');
        }
        
        if((receiver.length != 0) && (value.length != 0))
        {
            $('.modal-body #fwc_receiver_address').next(".text-danger").remove();
            $('.modal-body #fwc_amount').next(".text-danger").remove();
            
            doTokenTransfer(receiver, value).then(response => {
                console.log(response);
            }).catch(e => {
                if(e.data == null)
                {
                    document.getElementById("demo").innerHTML = e.message;   
                }
                else
                {
                    if((e.message != null) && (e.code == 4001))
                    {
                        document.getElementById("demo").innerHTML = e.message;  
                    }
                    else if(e.data != null)
                    {

                    document.getElementById("demo").innerHTML = e.data.message;   
                    }   
                }
            });         
        }
    });


    $("#btnSellFwc").click(function() {
        var receiver = $('#sellModal .modal-body #fwc_exchange_address').val();
        var usdtAddress = $('.modal-body #usdt_address').val();
        var value = $('#sellModal .modal-body #total_fwc').val();
        var resp = '';
        var hash = '';
        
        if(receiver.length == 0){
            $('#sellModal .modal-body #fwc_exchange_address').after('<div class="text-danger">*Receiver address is required</div>');
        }

        if(value.length == 0){
            $('#sellModal .modal-body #total_fwc').after('<div class="text-danger">*Amount is required</div>');
        }
        
        if((receiver.length != 0) && (value.length != 0))
        {
            $('#sellModal .modal-body #fwc_exchange_address').next(".text-danger").remove();
            $('#sellModal .modal-body #total_fwc').next(".text-danger").remove();
            
            doTokenTransfer(receiver, value, usdtAddress).then(response => {
                    var token = $("meta[name='csrf-token']").attr("content");
                    var resp = JSON.stringify(response);
                    var hash = response.hash;
                    
                    $.ajax({
                       type:'POST',
                       url:"{{ route('submit.sell') }}",
                       data:{usdt_address:usdtAddress, total_fwc:value, response:resp, tx_hash:hash, "_token": token},
                       success:function(data){
                        $('#sellModal').modal('hide');
                        $("#alert-trans-success").css("display", "");
                        
                        setTimeout(function () {
                            location.reload();
                         }, 4000);
                        
                       }
                    });
                
            }).catch(e => {
                if(e.data == null)
                {
                    document.getElementById("demo-sell").innerHTML = e.message;   
                }
                else
                {
                    if((e.message != null) && (e.code == 4001))
                    {
                        document.getElementById("demo-sell").innerHTML = e.message;  
                    }
                    else if(e.data != null)
                    {

                    document.getElementById("demo-sell").innerHTML = e.data.message;   
                    }   
                }
            });         
        }
    });

    async function login() {
          let user = Moralis.User.current();
          if (!user) {
            user = await Moralis.authenticate({
              signingMessage: "Log in using Moralis",
            })
              .then(function (user) {
                console.log("logged in user:", user);
                console.log(user.get("ethAddress"));
              })
              .catch(function (error) {
                console.log(error);
              });
          }
        }

    async function logOut() {
      await Moralis.User.logOut();
      console.log("logged out");
    }

    async function doTokenTransfer(receiver, value) {
        const web3 = await Moralis.enableWeb3();
        const options = {
          type: "erc20",
          amount: Moralis.Units.Token(value, "18"),
          receiver: receiver,
          contractAddress: "0x7d059fafbf681adbbf543d30260f3702d211b07e",
        };
        let result = Moralis.transfer(options);

        return result;
    }
   
    $("#total_usdt").keyup(function(){
      let valUsdt = $("#total_usdt").val();
      
      if((valUsdt < 0.0000000001) && (valUsdt < 0))
      {
        $("#total_usdt").val('');
        $("#fwc_received").html("");
      }
      else
      {
        $("#fwc_received").html("Your wallet will receive "+ valUsdt / {{$fwcUsdPrice}} +" FWC");
      }
    });

    $("#total_fwc").keyup(function(){
      let valFwc =  Number($("#total_fwc").val());
      console.log(valFwc);
      if((valFwc < 0.0000000001) && (valFwc < 0))
      {
        $("#total_fwc").val('');
      }

      if(valFwc > maxfwc) {
        valFwc = maxfwc;
        $("#total_fwc").val(maxfwc);
      }

      $("#usdt_received").html("Your wallet will receive "+ valFwc * {{$fwcSellPrice}} +" USDT");
    });

    $("#total_fwc").change(function(){
      let valFwc = Number($("#total_fwc").val());
      
      if((valFwc < 0.0000000001) && (valFwc < 0))
      {
        $("#total_fwc").val('');
      }

      if(valFwc > maxfwc) {
        valFwc = maxfwc;
        $("#total_fwc").val(maxfwc);
      }

      $("#usdt_received").html("Your wallet will receive "+ valFwc * {{$fwcSellPrice}} +" USDT");
    });

    $("#total_fwc").click(function(){
      let valFwc = Number($("#total_fwc").val());

      if((valFwc < 0.0000000001) && (valFwc < 0))
      {
        $("#total_fwc").val('')
      }

      if(valFwc > maxfwc) {
        valFwc = maxfwc;
        $("#total_fwc").val(maxfwc);
      }

      $("#usdt_received").html("Your wallet will receive "+ valFwc * {{$fwcSellPrice}} +" USDT");
    });


    $("#buyFwcUsdtButton").click(function() {
        var value = $('#buyModal .modal-body #total_usdt').val();
        var resp = '';
        var hash = '';

        if(value.length == 0){
            $('#buyModal .modal-body #total_usdt').after('<div class="text-danger">*Amount is required</div>');
        }
        
        if(value.length != 0)
        {
            $('#buyModal .modal-body #total_usdt').next(".text-danger").remove();
            
            doUsdtTransfer(value).then(response => {
                    var token = $("meta[name='csrf-token']").attr("content");
                    var resp = JSON.stringify(response);
                    var hash = response.hash;
                    
                    $.ajax({
                       type:'POST',
                       url:"{{ route('submit.buy') }}",
                       data:{total_usdt:value, response:resp, tx_hash:hash, "_token": token},
                       success:function(data){
                        $('#buyModal').modal('hide');
                        $("#alert-trans-success").css("display", "");
                        
                        setTimeout(function () {
                            location.reload();
                         }, 4000);
                        
                       }
                    });
                
            }).catch(e => {
                if(e.data == null)
                {
                    document.getElementById("demo-buy").innerHTML = e.message;   
                }
                else
                {
                    if((e.message != null) && (e.code == 4001))
                    {
                        document.getElementById("demo-buy").innerHTML = e.message;  
                    }
                    else if(e.data != null)
                    {

                    document.getElementById("demo-buy").innerHTML = e.data.message;   
                    }   
                }
            });         
        }
    });


    async function doUsdtTransfer(value) {
        const web3 = await Moralis.enableWeb3();
        var options = null;

        options = {
          type: "erc20",
          amount: Moralis.Units.Token(value,"18"),
          receiver: '{{$usdtExchangeAddress}}',
          contractAddress: "0x55d398326f99059ff775485246999027b3197955",
        };
        
        let result = Moralis.transfer(options);

        return result;
    }
</script>

@endsection
