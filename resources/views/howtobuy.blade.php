@extends('layouts.public')

@section('content')

    @include('layouts.public_header')
         <section class="fwc-banner-inside how-to-bg">
         <div class="container-fluid centered">
            <div class="row d-flex">
               <div class="col-md-7" data-aos="fade-up">
                  <div class="fwc-banner-text">
                     <h2>How to Buy</h2>
                     <p class="my-4">
                        Start following all the steps instructions belowif you're using metamask wallet to get FWC Token
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      
      <section class="fwc-metamask">
         <div class="container-fluid centered">
            <div class="row">
               <div class="col-md-12 text-center" data-aos="fade-up">
                  <h3> Metamask Mechanism</h3>
                  <h6 class="fwc-subtitle-blue">Start following these steps if you're using metamask wallet.</h6>
                  
               </div>
            </div>
            <div class="row my-3 d-flex justify-content-center">
               <div class="col-md-12" data-aos="fade-up">
                <p class="text-center">Please fill in the detail on stage 4 accordingly and don't forget to Insert this address to RPC URL box :</p>

                  <p class="d-flex justify-content-center">
                     <span class="wallet-code">https://speedy-nodes-nyc.moralis.io/85651f3e6612d7d97bf48571/bsc/mainnet</span>
                  </p>

                  <div class="d-flex justify-content-between step-01-metamask">
                    <div><span class="number">1</span> <img src="assets/img/metamask-step-01.png" /></div>
                    <img src="assets/img/arrow-blue.png" class="arrow-blue" />
                    <div><span class="number">2</span> <img src="assets/img/metamask-step-02.png" /></div>
                    <img src="assets/img/arrow-blue.png" class="arrow-blue"/>
                    <div><span class="number">3</span> <img src="assets/img/metamask-step-03.png" /></div>
                    <img src="assets/img/arrow-blue.png" class="arrow-blue"/>
                    <div><span class="number">4</span> <img src="assets/img/metamask-step-04.png" /></div>
                  </div>
               </div>
            </div>

            <div class="row my-5">
               <div class="col-md-12 text-center" data-aos="fade-up">
                  <h3> How to get the involve</h3>
                  <p>Follow these five key steps to ensure you have to</p>
                     <div class="d-flex justify-content-center">
                    <p class="fwc-prices">1 FWC = 25 USDT ( Pre-sale Price)</p>
                  </div>
                  <ul class="how-to-involved">
                    <li>
                        <span class="number">1</span>
                        Use your Metamask account to login through our website www.futureworkscoin.com
                    </li>
                    <li>
                        <span class="number">2</span>
                        Check that your Metamask network is using the smart chain network. If not, follow the Metamask Mechanism Instruction to change this setting
                    </li>
                    <li>
                        <span class="number">3</span>
                        Click the buy button on your dashboard and follow the instructions
                    </li>
                    <li>
                       <span class="number">4</span>
                        Once the order has been processed, you will then find the status of your transaction in the table below your dashboard
                    </li>
                    <li>
                       <span class="number">5</span>
                        After your transaction status has been approved click the " Add FWC to Metamask" in your dashboard to import the FWC token
                    </li>
                  </ul>
               </div>
            </div>
            <div class="row d-flex justify-content-center">
               <div class="col-md-10 text-center" data-aos="fade-up">
                  <h3> Metamask Mechanism</h3>
                  <h6 class="fwc-subtitle-blue">Start following these steps to import your token to your Metamask wallet after the purchase</h6>
                  
               </div>
            </div>
            <div class="row my-3 d-flex justify-content-center">
               <div class="col-md-12" data-aos="fade-up">
                <p class="text-center">Please follow these steps and input the detail address below in token adress box:</p>

                   <p class="d-flex justify-content-center">
                     <span class="wallet-code">0x7d059fafbf681adbbf543d30260f3702d211b07e</span>
                  </p>

                    <div class="d-flex justify-content-center step-01-metamask">
                    <div><span class="number">1</span> <img src="assets/img/metamask-06.png" /></div>
                    <img src="assets/img/arrow-blue.png" class="arrow-blue" />
                    <div><span class="number">2</span> <img src="assets/img/metamask-07.png" /></div>
                    <img src="assets/img/arrow-blue.png" class="arrow-blue"/>
                    <div><span class="number">3</span> <img src="assets/img/metamask-08.png" /></div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="fwc-terms">
         <div class="container-fluid centered">
            <div class="row">
               <div class="col-md-6 d-flex align-items-between flex-wrap" data-aos="fade-up">
                 <div> <h3>Terms and Condition</h3>
                  <p>By TRANSFER you acknowledge and agree that you have read, understand, accept all terms, and conditions contained in these Terms.</p></div> 
                  <img src="assets/img/fwc-blockchain.png" />
               </div>

               <div class="col-md-6" data-aos="fade-up">
                  <ul>
                     <li>
                        <span class="number">1</span>
                        FUTUREWORKS token works to increase digital assets within a certain period according to strict standards, and experienced analysis through systems that have proven success;

</li>
                     <li>
                        <span class="number">2</span>The value of Digital Assets can fluctuate very quickly, so be ready to accept the risk of losing all the money you put into the Digital Asset;</li>
                     <li>
                        <span class="number">3</span>Need to transfer the latest price to FUTUREWORKS token which can be seen at  <a href="">www.futureworkscoin.com;</a></li>
                     <li>
                        <span class="number">4</span>You are fully responsible for complying with all Applicable Laws relating to activities, and other uses of the Service, including without limitation, any reporting, and payment of all applicable taxes, and full compliance with any applicable anti-money laundering, sanctioning laws, and regulations. Without releasing you from responsibility for compliance with tax reporting, and payment obligations;</li>
                  </ul>
               </div>
            </div>

         </div>
      </section>
    @include('layouts.public_footer') 
@endsection