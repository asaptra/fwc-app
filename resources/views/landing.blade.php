@extends('layouts.public')

@section('content')

      @include('layouts.public_header')
    
      <section class="fwc-banner-home">
         <div class="container-fluid">
            <div class="row d-flex">
               <div class="col-md-6 " data-aos="fade-right">
                  <div class="fwc-banner-text">
                     <h6 class="fwc-subtitle-blue">The Future is Here <div id="google_translate_element"></div></h6>
                     <h2>Create Future Investments with Ease</h2>
                     <p class="my-4">
                        Virtual coin backed by strong underlying portfolio, enabling the network to achieve more stable and sustainable for the investment in the future.  
                     </p>
                     <div class="d-flex">
                        <a href="/home" class="btn btn-purple me-3">Buy Coin</a>
                        <a href="/how-to-buy" class="btn btn-white">How to Buy</a>
                     </div>
                  </div>
               </div>
               <div class="col-md-6" data-aos="fade-up">
                  <div class="row d-flex">
                     <div class="col-md-6">
                        <div class="fwc-total-coin">
                           <h6>Total Coin</h6>
                           <h3>4 Millions FWC</h3>
                           <p>The amount of tokens that are circulating in the market and are in public hands.</p>
                           <div class="remaining-coin">
                              <p class="mb-0">Remaining Tokens</p>
                              <h3>{{number_format(getRemainingToken(),3,".",",") }} FWC</h3>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6"  data-aos="fade-up">
                        <div class="fwc-price-coin">
                           <h6>Current Price</h6>
                           <h3>25 USDT <span class="presales-tag">Pre-sale Price</span></h3>
                           <p>The amount of price for you to buy 1 (one) coin of FutureWorksCoin
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="fwc-future-is my-5">
         <div class="container-fluid">
            <div class="row">
               <div class="col-md-6"  data-aos="fade-right"><img src="assets/img/fwc-future-is.png" /><img src="assets/img/fwc-future-is-coin.png" class="floating" /></div>
               <div class="col-md-6"  data-aos="fade-left">
                  <h3>The Futureworkscoin Token started with a clear vision to create innovative ways for investors to see tangible benefits in an ethical way; helping you earn money that is backed by a diversified portfolio.</h3>
                  <p>The mission behind our project reflects the desire to create virtual token backed by a strong underlying portfolio, enabling the network to achieve more stability and sustainability for investing in the future. Using quant-based technology for our underlying portfolio enables us to grow future projects and minimize your investment risk.</p>
                  <a href="/about" class="btn btn-purple my-3">LEARN MORE</a>
               </div>
            </div>
         </div>
      </section>
      <section class="fwc-we-offer my-5">
         <div class="container-fluid">
            <div class="row">
               <div class="col-md-12 text-center" data-aos="fade-up">
                  <h6 class="fwc-subtitle-blue">WHAT WE OFFER TO YOU</h6>
                  <h3>Gain biggest reward with your very low impact risk is made possible. </h3>
               </div>
            </div>
            <div class="row">
               <div class="col-md-4" data-aos="fade-up">
                  <div class="fwc-box-gradient">
                     <img src="assets/img/icon-growth.png" />
                     <p>Growth</p>
                     <p>Everyone wants to grow their wealth portfolio, which is why so many take measured risks and diversify through a variety of investment channels. FWC aims to create a more stable marketplace where our partners can gain significant returns in a safer environment
</p>
                  </div>
               </div>
               <div class="col-md-4" data-aos="fade-up" 
                  data-aos-delay="200">
                  <div class="fwc-box-gradient">
                     <img src="assets/img/icon-transparancy.png" />
                     <p>Transparency and Safety</p>
                     <p>Understanding how your investment is utilized is important. Our commitment to transparency means partners have access to regular updates - keeping you informed and helping you sleep better at night while your investment is growing</p>
                  </div>
               </div>
               <div class="col-md-4" data-aos="fade-up" 
                  data-aos-delay="400">
                  <div class="fwc-box-gradient">
                     <img src="assets/img/icon-market-liquidation.png" />
                     <p>Market Liquidation</p>
                     <p>FWC offers an easily accessible investment through deposit and withdrawal - backed by a buy-back mechanism.

  </p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="fwc-features my-5">
         <div class="container-fluid centered">
            <div class="row">
               <div class="col-md-12 text-center" data-aos="fade-up">
                  <h3>FWC Features </h3>
                  <ul class="list-features d-flex justify-content-between">
                     <li>
                        <img src="assets/img/features-01.png" />
                        <h4>Digital Asset</h4>
                     </li>
                     <li>
                        <img src="assets/img/features-02.png" />
                        <h4>Unlimited  
Scalibility</h4>
                     </li>
                     <li>
                        <img src="assets/img/features-03.png" />
                        <h4>Exchanger </h4>
                     </li>
                     <li>
                        <img src="assets/img/features-04.png" />
                        <h4>Utility token</h4>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </section>
      <section class="fwc-milestone-landing">
         <div class="container-fluid centered">
            <div class="row ">
               <div class="col-md-5" data-aos="fade-left">
 <h3>Our Milestone
                  </h3>
                  <p>Projects that we are focus on
                  </p>

                        <a href="/milestone" class="btn btn-purple me-3">MILESTONE DETAILS</a>
                  <img src="assets/img/fwc-stability.png" />
               </div>
               <div class="col-md-7" data-aos="fade-right">
                 <ul class="milestone-vertical">
                    <li>
                       <span class="number">1</span>
                       <h4>Independent finance ecosystem</h4>
                    </li>
                    <li>
                       <span class="number">2</span>
                       <h4>Marketplace</h4>
                    </li>
                    <li>
                       <span class="number">3</span>
                       <h4>Utility token in education</h4>
                    </li>
                    <li>
                       <span class="number">4</span>
                       <h4>Education metaverse smart contract</h4>
                    </li>
                    <li>
                       <span class="number">5</span>
                       <h4>Utility token in philanthropic project</h4>
                    </li>
                 </ul>
               </div>
            </div>
         </div>
      </section>
      <section class="fwc-how-to" >
         <div class="container-fluid centered">
            <div class="row">
               <div class="col-md-12 text-center" data-aos="fade-up">
                  <h6 class="fwc-subtitle-blue">GET INVOLVED</h6>
                  <h3>Get FWC Token</h3>
               </div>
            </div>
            <div class="row my-5">
               <div class="col-md-6 text-center bg-blur-blue" data-aos="fade-right">
                  <img src="assets/img/fwc-how-to.png" />
               </div>
               <div class="col-md-6" data-aos="fade-left">

                    <p class="fwc-prices">1 FWC = 25 USDT ( Pre-sale Price)</p>
                  <p>We allowed people to be part of a shared mission and to have a full control of their funds with our buy back mechanism that will keep the liquidity of the assets</p>

                  <p>Start following all the steps instruction in ‘How to buy’ page if you're using metamask wallet to get FWC Token</p>
                  <a href="/how-to-buy" class="btn btn-purple">HOW TO BUY</a>
               </div>
            </div>
         </div>
      </section>
      

      @include('layouts.public_footer')

@endsection
