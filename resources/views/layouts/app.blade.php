<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Futureworkscoin - Dashboard</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css"
         href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
         integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
      <!-- Core theme CSS (includes Bootstrap)-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <link rel="stylesheet" href="assets/css/font.css">
    <link rel="stylesheet" href="assets/css/dashboard.css">
</head>
<body>
    <section id="wrapper">
     <section id="sidebar">
        <a href="#" class="toggle-sidebar"><i class="icon-ic-arrow-caret-right-Stroke"></i></a>
        <div class="content-sidebar">
           <a href="/" class="logo">
              <img src="assets/img/logo-dashboard.png" />
              <img class="logo-icon" src="" />
           </a>
           <div class="profile-info">
              <div class="ava-profile">
               @if(Auth::user()->image_name)
                 <img src="photo/profile/images/{{Auth::user()->image_name}}"/>
               @else
                 <img src="assets/img/ava-profile.png"/>
               @endif
              </div>
              <p>@if(Auth::user()) {{ Auth::user()->name }} @endif</p>
           </div>
           <div class="user-wallet">
              <p>Your Wallet:</p>
              <h5>@if(Auth::user()) {{ Auth::user()->fwc_address }} @endif</h5>
           </div>
           <ul class="sidebar-menu">
              <li>
                 <a href="/home" class="active"><i class="icon-ic-dashboard"></i><span>Dashboard</span></a>
              </li>
               <li>
                  <a href="#" onclick="return confirm('Coming soon!')"><i class="icon-ic-marketplace text-secondary"></i><span class="text-secondary">Marketplace</span></a>
               </li>
               <li>
                  <a href="#" onclick="return confirm('Coming soon!')"><i class="icon-ic-education text-secondary"></i><span class="text-secondary">Education</span></a>
               </li>
               <li>
                  <a href="#" onclick="return confirm('Coming soon!')"><i class="icon-ic-donate text-secondary"></i><span class="text-secondary">Donate</span></a>
               </li>
               <li>
                  <a href="#" onclick="return confirm('Coming soon!')"><i class="icon-ic-wallet text-secondary"></i><span class="text-secondary">Wallet</span></a>
               </li>
               <li>
                  <a href="#" onclick="return confirm('Coming soon!')"><i class="icon-ic-stack text-secondary"></i><span class="text-secondary">Stake</span></a>
               </li>
               <li>
                  <a href="#" onclick="return confirm('Coming soon!')"><i class="icon-ic-supply text-secondary"></i><span class="text-secondary">Supply</span></a>
               </li>
               <li>
                  <a href="#" onclick="return confirm('Coming soon!')"><i class="icon-ic-history text-secondary"></i><span class="text-secondary">History</span></a>
               </li>
           </ul>
        </div>
     </section>
     <section id="main">
        @include('layouts.app_nav')

        @if ($message = Session::get('success'))
         <div class="alert alert-success alert-dismissible fade show" role="alert">
           <strong>Notice success: </strong> {{ $message }}
           <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
         </div>
         @endif

         @if ($message = Session::get('failed'))
         <div class="alert alert-danger alert-dismissible fade show" role="alert">
           <strong>Notice failed: </strong> {{ $message }}
           <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
         </div>
         @endif

         <div class="alert alert-success alert-dismissible fade show" role="alert" id="alert-trans-success" style="display:none">
           <strong>Notice success: </strong> Your transaction is being process.
           <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
         </div>

        @yield('content')
     </section>
    </section>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- Bootstrap core JS-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script src="assets/js/dashboard.js"></script>
    <script src="assets/js/main.js"></script>
</body>
</html>