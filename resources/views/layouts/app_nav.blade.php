<nav>
  <div class="d-flex justify-content-between align-items-baseline flex-row-reverse">
     <div class="d-flex justify-content-end">
         <div class="dropdown account">
              <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
              
          <div class="ava-profile">
            @if(Auth::user()->image_name)
              <img src="photo/profile/images/{{Auth::user()->image_name}}"/>
            @else
              <img src="assets/img/ava-profile.png"/>
            @endif
           </div>
           <p>@if(Auth::user())  {{ Auth::user()->name }} @endif</p>
           <i class="icon-ic-arrow-caret-down-Stroke"></i>
              </button>
              <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton1">
           <a class="nav-link" href="/profile">My Profile</a>
           <a class="nav-link" href="/referral">My Referral</a>
            @if(Auth::user()->roles == 'admin')
           <li><hr class="dropdown-divider"></li>
           <a class="nav-link" href="/transactions">Transaction</a>
           <a class="nav-link" href="/config-price">Config Price</a>
           @endif
           <li><hr class="dropdown-divider"></li>
           <a class="nav-link" href="{{ route('logout') }}"
              onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
               {{ __('Logout') }}
           </a>

           <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
               @csrf
           </form>

              </div>
           </div>
     </div>
     <h2>Welcome to FWC, @if(Auth::user()) {{ Auth::user()->name }} @endif</h2>
  </div>
</nav>