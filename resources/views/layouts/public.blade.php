<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
      <meta name="description" content="A Virtual coin backed by strong underlying portfolio, enabling the network to achieve more stable and sustainable for the investment in the future." />
      <meta name="author" content="futureworkscoin" />
      <meta property="og:image" itemprop="image" content="https://www.futureworkscoin.com/assets/img/fwc-future-is-coin.png">
      <meta property="og:type" content="website" />
      <meta property="og:image:type" content="image/png">
      <meta property="og:image:width" content="300">
      <meta property="og:image:height" content="300">
      <meta property="og:url" content="https://www.futureworkscoin.com">

      <meta name="csrf-token" content="{{ csrf_token() }}" />
      <title>Futureworkscoin</title>
      <!-- Favicon-->
      <!-- <link rel="icon" type="image/x-icon" href="assets/favicon.ico"/> -->
      <!-- Bootstrap icons-->
      <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet" />
      <link rel="stylesheet" type="text/css"
         href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
         integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
      <!-- Core theme CSS (includes Bootstrap)-->
      <link href="../assets/css/bootstrap.css" rel="stylesheet" />
      <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
      <link rel="stylesheet" href="../assets/css/main.css">
      <style type="text/css">
         #goog-gt-tt, .goog-te-balloon-frame{display: none !important;}
         .goog-text-highlight { background: none !important; box-shadow: none !important;}
         .goog-te-banner-frame{
          display:none !important;
          }
           .goog-logo-link{
             display:none !important;
            }
          .goog-te-gadget{
           color:transparent!important;
           /*display: none;*/
           }

           #google_translate_element select{
             background: transparent;
             font-weight: bold;
             color:#FFFFFF;
             font-size: 14px;
             border: none;
             margin-top: 9px;
             font-family: Inter;
             width: 119px;
             }
             .lang-google
             {
               position: relative;
             }
      </style>

      
<script src="https://unpkg.com/@metamask/detect-provider/dist/detect-provider.min.js"></script>

   </head>
   <body>
      @yield('content')

      <script type="text/javascript">
        function googleTranslateElementInit() {
          new google.translate.TranslateElement({  
            // pageLanguage: 'en', 
            // layout: google.translate.TranslateElement.InlineLayout.SIMPLE,
            includedLanguages: "en,id,ja,zh-CN,ko,es"
          }, 'google_translate_element');
        }
      </script>
      <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
      <script>
        $(document).ready(function(){
          $('#google_translate_element').hide();
          $('#google_translate_element').bind('DOMNodeInserted', function(event) {
            $('.goog-te-menu-value span:first').html('Translate');
            $('.goog-te-menu-frame.skiptranslate').load(function(){
              setTimeout(function(){
                $('.goog-te-menu-frame.skiptranslate').contents().find('.goog-te-menu2-item-selected .text').html('Translate');    
              }, 100);
            });
          });
        });
        

        setTimeout(
          function() 
          {
            $(".goog-te-combo option[value='en']").text("English");
            $(".goog-te-combo option[value='id']").text("Indonesia");
            $(".goog-te-combo option[value='ja']").text("Japanese");
            $(".goog-te-combo option[value='ko']").text("Korean");
            $(".goog-te-combo option[value='es']").text("Spanish");
            $(".goog-te-combo option[value='zh-CN']").text("Chinese");  
            $('#google_translate_element').show();
          }, 1000);

        function selectedlang()
        {
          setTimeout(
          function() 
          {
            $(".goog-te-combo option[value='en']").text("English");
            $(".goog-te-combo option[value='id']").text("Indonesia");
            $(".goog-te-combo option[value='ja']").text("Japanese");
            $(".goog-te-combo option[value='ko']").text("Korean");
            $(".goog-te-combo option[value='es']").text("Spanish");
            $(".goog-te-combo option[value='zh-CN']").text("Chinese");  
            $('#google_translate_element').show();
          }, 2000);
        }

      </script>
   </body>
</html>