<footer data-aos="fade-up">
   <div class="container-fluid centered">
      <div class="row">
         <div class="col-md-12 text-center">
            <h3>Get your FWC token today!</h3>
            <p>We believe you smarter then that to make a good choices
            We only provide 4 Million token grab it fast!
            </p>
            <a href="/home" class="btn btn-purple me-3">Buy Coin</a>
            <a href="/how-to-buy" class="btn btn-white">How to Buy</a>
            <ul class="social-media">
               <li>
                  <a href="">
                  <img src="assets/img/ic-telegram.png" />
                  <span>Telegram</span>
                  </a>
               </li>
               <li>
                  <a href="">
                  <img src="assets/img/ic-twitter.png" />
                  <span>Twitter</span>
                  </a>
               </li>
               <li>
                  <a href="">
                  <img src="assets/img/ic-reddit.png" />
                  <span>Reddit</span>
                  </a>
               </li>
               <li>
                  <a href="">
                  <img src="assets/img/ic-discord.png" />
                  <span>Discord</span>
                  </a>
               </li>
               <li>
                  <a href="">
                  <img src="assets/img/ic-ig.png" />
                  <span>Instagram</span>
                  </a>
               </li>
            </ul>
         </div>
      </div>
      <div class="row d-flex justify-content-between copyright">
         <div class="col-md-4">
            <p>Copyright © FutureWorksCoin 2022</p>
         </div>
         <div class="col-md-4"><a href="">Terms & Conditions</a> <a href="">Privacy Policy</a></div>
      </div>
   </div>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!-- Bootstrap core JS-->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
<script src="assets/js/main.js"></script>