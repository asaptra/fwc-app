<nav class="navbar navbar-expand-lg fwc-navbar-front">
   <div class="container-fluid">
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
         <span class="navbar-toggler-icon">
            <div id="nav-icon1">
               <span></span>
               <span></span>
               <span></span>
            </div>
         </span>
      </button>
      <a class="navbar-brand" href="/"><img src="../assets/img/logo-horizontal-fwc.svg" /></a>
      <div class="d-flex justify-content-end">
         <div class="collapse navbar-collapse" id="navbarScroll">
            <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" >
               <li class="nav-item">
                  <a class="nav-link {{ request()->is('about') ? 'active' : '' }}" aria-current="page" href="/about">About</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link {{ request()->is('roadmap') ? 'active' : '' }}" href="/roadmap">Roadmaps</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link {{ request()->is('milestone') ? 'active' : '' }}" href="/milestone">Milestone</a>
               </li>
               <li class="nav-item">
                  <div class="dropdown">
                     <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                     <span>How to Buy</span>
                     </button>
                     <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton1">
                  <a class="nav-link" href="/how-to-buy">Buying Instruction</a>
                  <a class="nav-link" href="/login">Launching</a>
                     </div>
                  </div>
               </li>
               <li class="nav-item">
                  <a class="nav-link {{ request()->is('faq') ? 'active' : '' }}" href="/faq">FAQ</a>
               </li>
               <li>
                   <div class="lang-google d-flex">
                        <span id="google_translate_element" onclick="selectedlang()"></span>
                  </div>
               </li>
               
            </ul>
         </div>
        
         <div class="d-flex">
            @if(Auth::user())
            <a class="btn btn-outline" href="/home">Dashboard</a>
            @else
            <a class="btn btn-link" href="/login">Login</a>
            <a class="btn btn-outline" href="/register">Register</a>
            @endif
         </div>
      </div>
   </div>
</nav>