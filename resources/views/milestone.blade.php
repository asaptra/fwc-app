@extends('layouts.public')

@section('content')
   @include('layouts.public_header')

    <section class="fwc-banner-inside milestone-bg">
         <div class="container-fluid centered">
            <div class="row d-flex">
               <div class="col-md-7" data-aos="fade-up">
                  <div class="fwc-banner-text">
                     <h6 class="fwc-subtitle-blue">Projects that we are focus on</h6>
                     <h2>Our Milestone</h2>
                     <p class="my-4">
                       This project milestone helps us share what we’re working on next and the direction that we’re taking our business and focus on. 
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="fwc-timeline-milestone">
         <div class="container-fluid ">
            <div class="row">
               <div class="col-md-12" data-aos="fade-up">
                  <ul class="milestone-menu" id="milestone-nav">
                     <li ><a class="nav-link" href="#milestone-01">Independent finance ecosystem <span>1</span></a></li>
                     <li><a class="nav-link" href="#milestone-02">Marketplace <span>2</span></a></li>
                     <li><a class="nav-link" href="#milestone-03">Utility token in Education <span>3</span></a></li>
                     <li><a class="nav-link" href="#milestone-04">Education metaverse
                        smart contract <span>4</span></a>
                     </li>
                     <li><a class="nav-link" href="#milestone-05">Utility token in philanthropic project <span>5</span></a></li>
                  </ul>
               </div>
            </div>
         </div>
      </section>
      <section data-bs-spy="scroll" data-bs-target="#milestone-nav" data-bs-offset="200">
         <section class="fwc-milestone-box" id="milestone-01">
            <div class="container-fluid centered">
               <div class="row flex-row-reverse">
                  <div class="col-md-6" data-aos="fade-left"><img src="assets/img/milestone-01.png" /></div>
                  <div class="col-md-6" data-aos="fade-right">
                     <h6 class="fwc-subtitle-blue">Super-App to cater all your virtual token transactions</h6>
                     <h3>Independent
                        Finance Ecosystem
                     </h3>
                     <p>One of Futureworks token projects is the launch of a payment processing method for the FWC token that will be connected to everyone's wallet and enable them to purchase and sell their token, including looking at the live report of their token performance
 
                     </p>
                  </div>
               </div>
            </div>
         </section>
         <section class="fwc-milestone-box" id="milestone-02">
            <div class="container-fluid centered">
               <div class="row">
                  <div class="col-md-6" data-aos="fade-right"><img src="assets/img/milestone-02.png" /></div>
                  <div class="col-md-6" data-aos="fade-left">
                     <h6 class="fwc-subtitle-blue">Marketplace for investing in cryptocurrencies.</h6>
                     <h3>Marketplace
                     </h3>
                     <p>Future Works token will launch a complete marketplace for cryptocurrency using FWC for investments in other assets through utility token. We have posistioned FWC to help increase the amount of people able to join the investor class.</p>
                  </div>
               </div>
            </div>
         </section>
         <section class="fwc-milestone-box" id="milestone-03">
            <div class="container-fluid centered">
               <div class="row flex-row-reverse">
                  <div class="col-md-6" data-aos="fade-left"><img src="assets/img/milestone-03.png" /></div>
                  <div class="col-md-6" data-aos="fade-right">
                     <h3>Utility token in Education
                     </h3>
                     <p>We believe accessibility to quality education should be universal. With education costs set to rise, students are facing tough decisions to be able to afford the education they deserve.
FWC was created to bring a solution to this problem through our staking system that will allow our token holders to pay for their education without sacrificing their investment portfolio and continue growth.
 
                     </p>
                  </div>
               </div>
            </div>
         </section>
         <section class="fwc-milestone-box" id="milestone-04">
            <div class="container-fluid centered">
               <div class="row">
                  <div class="col-md-6" data-aos="fade-right"><img src="assets/img/milestone-04.png" /></div>
                  <div class="col-md-6" data-aos="fade-left">
                     <h6 class="fwc-subtitle-blue">The Metaverse World of Education</h6>
                     <h3>Education metaverse
                        smart contract
                     </h3>
                     <p>The future of education is in the metaverse, which is becoming exponetionally prevalent to transform education ecosystem toward being incorporated.</p>
<p>
One of the utility tokens under FWC will be connected to the development of the educational metaverse through a smart contract.</p>
                  </div>
               </div>
            </div>
         </section>
         <section class="fwc-milestone-box" id="milestone-05">
            <div class="container-fluid centered">
               <div class="row flex-row-reverse">
                  <div class="col-md-6" data-aos="fade-left"><img src="assets/img/milestone-05.png" /></div>
                  <div class="col-md-6" data-aos="fade-right">
                     <h6 class="fwc-subtitle-blue">A charitable fund built in the cryptocurrencies world</h6>
                     <h3>Utility token for
                        philanthropic projects
                     </h3>
                     <p>Part of the brand promise of FWC is to make a meaningful contribution to society and give back to people through a sustainable nonprofit program that will be funded by our FWC staking system for philanthropists who entrust their funds to support many indepedant nonprofit projects.
                     </p>
                  </div>
               </div>
            </div>
         </section>
      </section>

   @include('layouts.public_footer')

   <script type="text/javascript">
         var scrollSpy = new bootstrap.ScrollSpy(document.body, {
         target: '#milestone-nav',
         offset: 200
         })
         
         var fixmeTop = $('.milestone-menu').offset().top;       // get initial position of the element
         
         $(window).scroll(function() {                  // assign scroll event listener
         
         var currentScroll = $(window).scrollTop(); // get current position
         
         if (currentScroll >= fixmeTop) {           // apply position: fixed if you
         $('.milestone-menu').addClass('fixed');
         } else {                                   // apply position: static
         $('.milestone-menu').removeClass('fixed');
         }
         
         });
      </script>
@endsection