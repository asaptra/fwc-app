@extends('layouts.public')

@section('content')
   @include('layouts.public_header')

    <section class="fwc-banner-inside roadmap-bg">
         <div class="container-fluid centered">
            <div class="row d-flex">
               <div class="col-md-7" data-aos="fade-up">
                  <div class="fwc-banner-text">
                     <h6 class="fwc-subtitle-blue">Our Roadmap for Growth</h6>
                     <h2>Roadmaps</h2>
                     <p class="my-4">
                        With this in mind, we have produced 'Our Roadmap for Growth'. This roadmap defines our objectives, how we will achieve them and what success will look like.

                     </p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="roadmap-timeline">
         <div class="container-fluid centered">
            <div class="row">
               <div class="col-md-12">
                  <div class="steps">
                     <div class="steps-container">
                        <div class="content" data-aos="fade-right">
                           <h2>Private Sale</h2>
                           <ul>
                              <li>Pre-sale token</li>
                              <li>Community deployment  </li>
                              <li>First Phase of Website development</li>
                           </ul>
                        </div>
                        <div class="date" data-aos="fade-left">2022</div>
                        <div class="quartal"  data-aos="fade-up"><span>Q1</span></div>
                     </div>
                     <div class="steps-container">
                        <div class="content" data-aos="fade-right">
                           <h2>Launching</h2>
                           <ul>
                              <li> Soft Launching  </li>
                              <li>
                                 Website enhancement  
                              </li>
                              <li>
                                 Deploy Mobile App project
                              </li>
                           </ul>
                        </div>
                        <div class="date" data-aos="fade-left">2022</div>
                        <div class="quartal"  data-aos="fade-up"><span>Q2</span></div>
                     </div>
                     <div class="steps-container">
                        <div class="content" data-aos="fade-right">
                           <h2>Exchanger 1.0</h2>
                           <ul>
                              <li> Mobile apps 2.0  </li>
                              <li>Testing the marketplace function   </li>
                              <li>Deploy the Beta version of marketplace </li>
                              <li>Initiating the Utility project based function</li>
                           </ul>
                        </div>
                        <div class="date" data-aos="fade-left">2022</div>
                        <div class="quartal" data-aos="fade-up"><span>Q3</span></div>
                     </div>
                     <div class="steps-container">
                        <div class="content" data-aos="fade-right">
                           <h2>Exchanger 2.0</h2>
                           <ul>
                              <li> Pre-launch Marketplace</li>
                              <li>
                                 Pilot the utility based coin  
                              </li>
                              <li>
                                 Build the beta website
                              </li>
                              <li>
                                 Initiation Education utility based project coin.
                              </li>
                           </ul>
                        </div>
                        <div class="date" data-aos="fade-left">2022</div>
                        <div class="quartal"  data-aos="fade-up"><span>Q4</span></div>
                     </div>
                     <div class="steps-container">
                        <div class="content" data-aos="fade-right">
                           <h2>Utility based coin</h2>
                           <ul>
                              <li>Pre-sale uitility based coin</li>
                              <li>
                                 Initiation CSR utiliy based coin  
                              </li>
                              <li>
                                 Smart contract
                              </li>
                           </ul>
                        </div>
                        <div class="date" data-aos="fade-left">2023</div>
                        <div class="quartal"  data-aos="fade-up"><span>Q1</span></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

   @include('layouts.public_footer')
@endsection