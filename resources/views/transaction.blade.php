@extends('layouts.app')

@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>    
<script src="https://unpkg.com/moralis/dist/moralis.js"></script>


@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
  <strong>Notice success: </strong> {{ $message }}
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

@if ($message = Session::get('failed'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
  <strong>Notice failed: </strong> {{ $message }}
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

<div class="row">
   <div class="col-md-12">
      <div class="card card-grey">
         <h6>LIST TRANSACTION (BUY/SELL)</h6>
         <table class="table text-light text-center">
             <thead>
               <th>Trans Type</th>
               <th>Receiver Address</th>
               <th>Date</th>
               <th>USDT</th>
               <th>FWC</th>
               <th>Status</th>
               <th>Action</th>
             </thead>
              <tbody>
                @foreach($transactions as $t)
                <tr>
                    <td>
                        @if ($t->trans_type == 0)
                            <span class="badge bg-success">BUY</span>
                        @endif
                        @if ($t->trans_type == 1)
                            <span class="badge bg-danger">SELL</span>
                        @endif
                    </td>
                    <th>
                        @if ($t->trans_type == 0)
                            {{$t->fwc_address}}
                        @else
                            {{$t->usdt_address}}
                        @endif
                        @if($t->tx_hash)
                        <br/>
                        <a href="https://bscscan.com/tx/{{$t->tx_hash}}" target="_blank">Proof on Bsc Scan</a>
                        @endif
                    </th>
                    <td>{{$t->created_at}}</td>
                    <td>$ {{$t->total_usdt}}</td>
                    <td>{{$t->total_fwc_send}}</td>
                    <td>
                        @if ($t->status == 0)
                            <span class="badge bg-warning text-dark">On Progress</span>
                        @endif
                        @if ($t->status == 1)
                            <span class="badge bg-success">Completed</span>
                        @endif
                        @if ($t->status == 2)
                            <span class="badge bg-danger">Failed</span>
                        @endif
                    </td>
                    <td>
                        @if ($t->status != 1)
                            @if ($t->trans_type == 0)
                                 <span class="badge bg-danger"><a  href="#" class="text-light open-buy-transfer"  data-bs-toggle="modal" data-bs-target="#transferBuyModal" data-receiveraddress="{{$t->fwc_address}}" data-amount="{{$t->total_fwc_send}}" data-transtype="{{$t->trans_type}}" data-imageproof="{{$t->proof_image}}" data-idtrans="{{$t->id}}" data-txhash="{{$t->tx_hash}}">Transfer</a></span>
                            @endif
                            @if ($t->trans_type == 1)
                                 <span class="badge bg-danger"><a  href="#" class="text-light open-buy-transfer"  data-bs-toggle="modal" data-bs-target="#transferBuyModal" data-receiveraddress="{{$t->usdt_address}}" data-amount="{{$t->total_usdt}}" data-transtype="{{$t->trans_type}}" data-idtrans="{{$t->id}}" data-txhash="{{$t->tx_hash}}">Transfer</a></span>
                            @endif
                        @else
                        <span class="badge bg-success"><a href="https://bscscan.com/tx/{{$t->tx_admin}}" target="_blank" class="text-light">Proof Admin</a></span>
                        @endif
                        
                    </td>
                </tr>
                @endforeach
               
               </tbody>
         </table>
      </div>
   </div>
</div><!-- 
linear-gradient(180deg, #2F274D 0%, #222336 100%) -->

<div class="modal fade text-light" id="transferBuyModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" >
    <div class="modal-content" style="background-color: #29243b;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Transfer Token</h5>
        <button type="button" class="btn-close bg-light" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
       
      <div class="modal-body">
         <div class="form-group">
            <div class="d-flex justify-content-between"><label>Proof Transfer</label></div>
                <img src="" id="imgproof" height="400px"  width="300px" />
                <span id="proofdesc" style="font-style: italic;"></span>
            </div>
         <br/>
         <div class="form-group">
            <div class="d-flex justify-content-between"><label>Receiver Address</label></div>
            <input id="fwc_receiver_address" type="text" class="form-control text-secondary" name="fwc_receiver_address" required readonly="true">
         </div>
         <br/>
         <div class="form-group">
            <div class="d-flex justify-content-between"><label>Amount</label></div>
            <input id="fwc_amount" type="number" class="form-control text-secondary" name="fwc_amount" required readonly="true">
            <input id="trans_type" type="hidden" class="form-control text-secondary" name="trans_type" required readonly="true">
            <input id="id_trans" type="hidden" class="form-control text-secondary" name="id_trans" required>
         </div>
         <br/>

      <div id="demo" class="text-danger"></div>        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-purple" id="transferFwcButton">Transfer</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    $( document ).ready(function() {
        const serverUrl = "https://berx8b2icccr.usemoralis.com:2053/server";
        const appId = "WEOKYUkhg64mkzpPWpDdGiLv8EgcLbHzFAr0lByI";

        Moralis.start({ serverUrl, appId });
    });

    $("#transferFwcButton").click(function() {
        var receiver = $('.modal-body #fwc_receiver_address').val();
        var value = $('.modal-body #fwc_amount').val();
        var transType = $('.modal-body #trans_type').val();
        var idtrans = $('.modal-body #id_trans').val();
        var resp = '';
        var tx_admin = '';

        if(receiver.length == 0){
            $('.modal-body #fwc_receiver_address').after('<div class="text-danger">*Receiver address is required</div>');
        }

        if(value.length == 0){
            $('.modal-body #fwc_amount').after('<div class="text-danger">*Amount is required</div>');
        }
        
        if((receiver.length != 0) && (value.length != 0))
        {
            $('.modal-body #fwc_receiver_address').next(".text-danger").remove();
            $('.modal-body #fwc_amount').next(".text-danger").remove();
            
            doTokenTransfer(receiver, value, transType).then(response => {
                var resp = JSON.stringify(response);
                var tx_admin = response.hash;
                
                var token = $("meta[name='csrf-token']").attr("content");
                $.ajax({
                   type:'POST',
                   url:"{{ route('admin.transfer') }}",
                   data:{ id:idtrans, response:resp, tx_admin:tx_admin, amount:value, trans_type:transType, "_token": token},
                   success:function(data){
                    $('#transferBuyModal').modal('hide');
                    $("#alert-trans-success").css("display", "");
                    
                    setTimeout(function () {
                        location.reload();
                     }, 4000);
                    
                   }
                });
                

            }).catch(e => {
                if(e.data == null)
                {
                    document.getElementById("demo").innerHTML = e.message;   
                }
                else
                {
                    if((e.message != null) && (e.code == 4001))
                    {
                        document.getElementById("demo").innerHTML = e.message;  
                    }
                    else if(e.data != null)
                    {
                        document.getElementById("demo").innerHTML = e.data.message;   
                    }   
                }
                console.log('ember');
                console.log(resp);
                // var token = $("meta[name='csrf-token']").attr("content");
                // $.ajax({
                //    type:'POST',
                //    url:"{{ route('admin.transfer') }}",
                //    data:{ id:idtrans, response:resp, "_token": token},
                //    success:function(data){
                //     $('#transferBuyModal').modal('hide');
                //     $("#alert-trans-success").css("display", "");
                    
                //     setTimeout(function () {
                //         // location.reload();
                //      }, 4000);
                    
                //    }
                // });
                console.log('ember');
            });         
        }
    });

    async function login() {
          let user = Moralis.User.current();
          if (!user) {
            user = await Moralis.authenticate({
              signingMessage: "Log in using Moralis",
            })
              .then(function (user) {
                console.log("logged in user:", user);
                console.log(user.get("ethAddress"));
              })
              .catch(function (error) {
                console.log(error);
              });
          }
        }

    async function logOut() {
      await Moralis.User.logOut();
      console.log("logged out");
    }

    async function doTokenTransfer(receiver, value, transType) {
        const web3 = await Moralis.enableWeb3();
        var options = null;

        if(transType == 0)
        {
            options = {
              type: "erc20",
              amount: Moralis.Units.Token(value, "18"),
              receiver: receiver,
              contractAddress: "0x7d059fafbf681adbbf543d30260f3702d211b07e",
            };
        }
        else
        {
            options = {
              type: "erc20",
              amount: Moralis.Units.Token(value,"18"),
              receiver: receiver,
              contractAddress: "0x55d398326f99059ff775485246999027b3197955",
            };
        }

        let result = Moralis.transfer(options);

        return result;
    }
   
   $(document).on("click", ".open-buy-transfer", function () {
         var receiverAddress = $(this).data('receiveraddress');
         var amount = $(this).data('amount');
         var transType = $(this).data('transtype');
         var checkImage = $(this).data('imageproof');
         var idTrans = $(this).data('idtrans');
         var imageProff = "/photo/purchase/{{$t->fwc_address}}/"+$(this).data('imageproof');
         var txHash = $(this).data('txhash');

         $("#transferBuyModal .modal-body #fwc_receiver_address").val( receiverAddress );
         $("#transferBuyModal .modal-body #fwc_amount").val( amount );
         $("#transferBuyModal .modal-body #trans_type").val( transType );
         $("#transferBuyModal .modal-body #id_trans").val( idTrans );
         
         if(checkImage)
         {
             $("#proofdesc").show();
             $("#proofdesc").html(txHash);
         }
         else
         {
            $("#proofdesc").show();
            $("#proofdesc").html(txHash);
            $("#transferBuyModal .modal-body #imgproof").hide();
         }

    });
   

</script>

@endsection
