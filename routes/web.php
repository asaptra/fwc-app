<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/roadmap', function () {
    return view('roadmap');
});

Route::get('/milestone', function () {
    return view('milestone');
});

Route::get('/how-to-buy', function () {
    return view('howtobuy');
});

Route::get('/faq', function () {
    return view('faq');
});

Route::get('/loginbm', function () {
    return view('auth.loginbm');
});

Auth::routes();

Route::post('/login_metamask', [App\Http\Controllers\Auth\LoginController::class, 'loginMetamask'])->name('login.metamask');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/profile', [App\Http\Controllers\HomeController::class, 'profile'])->name('edit.profile');
Route::get('/edit_profile', [App\Http\Controllers\HomeController::class, 'editProfile'])->name('edit.profile.form');
Route::post('/submit_profile', [App\Http\Controllers\HomeController::class, 'submitProfile'])->name('submit.profile');
Route::get('/referral', [App\Http\Controllers\HomeController::class, 'referral'])->name('list.referral');
Route::post('/submit_referral', [App\Http\Controllers\HomeController::class, 'submitReferral'])->name('submit.referral');
Route::post('/change_password', [App\Http\Controllers\HomeController::class, 'changePassword'])->name('change.password');
Route::get('/reload-captcha', [App\Http\Controllers\Auth\RegisterController::class, 'reloadCaptcha']);
Route::post('/submit_buy', [App\Http\Controllers\HomeController::class, 'submitBuyFwc'])->name('submit.buy');
Route::post('/submit_sell', [App\Http\Controllers\HomeController::class, 'submitSellFwc'])->name('submit.sell');
Route::get('/transactions', [App\Http\Controllers\HomeController::class, 'transaction'])->name('list.transaction');
Route::post('/admin-transfer', [App\Http\Controllers\HomeController::class, 'adminTransfer'])->name('admin.transfer');

Route::get('/config-price', [App\Http\Controllers\HomeController::class, 'configPrice'])->name('configprice');
Route::post('/config-price', [App\Http\Controllers\HomeController::class, 'submitConfigPrice'])->name('submit.configprice');

